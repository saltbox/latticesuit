package com.saltbox.latticesuit.UI;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Menu extends UIGameObject {
	public ArrayList<String> options = new ArrayList<String>();
	public ArrayList<Vector2i> spriteOptions = new ArrayList<Vector2i>();
	protected int selectedOption;
	public boolean selected = true;
	public boolean horizontal = false;
	public boolean spriteMode = false;
	int previousOption;
	
	public String title;
	public IOptionColorer optionColorer;
	float hangTime = 0f;
	
	public IMenuOptionSelect menuOptionSelect;

	public Menu(Vector2f position, Vector2i size) {
		super(position, 8, size);
		optionColorer = new DefaultOptionColorer();
	}

	public Menu(Vector2f position) {
		this(position, new Vector2i(24, 20));
	}
	
	public Menu() {
		this(new Vector2f());
	}

	@Override
	public void Update(GameTime gameTime) {
		hangTime -= gameTime.getDeltaTime();
		if (hangTime <= 0f) {
			
			if (((!horizontal && KeyboardInputTools.up()) || (horizontal && KeyboardInputTools.left())) && selectedOption > 0) {
				setSelectedOption(getSelectedOption() - 1);
				hangTime = LatticeSuit.repeatInputHangTime;
			}

			if (((!horizontal && KeyboardInputTools.down()) || (horizontal && KeyboardInputTools.right()) )&& selectedOption < options.size() - 1) {
				setSelectedOption(getSelectedOption() + 1);
				hangTime = LatticeSuit.repeatInputHangTime;
			}
			
			if (KeyboardInputTools.confirm() && menuOptionSelect != null) {
				menuOptionSelect.call(selectedOption);
			}
			
			for (int i = 0; i < options.size() && i <= GLFW_KEY_9; i++) {
				if (KeyboardInput.isKeyDown(i + GLFW_KEY_1) && !KeyboardInput.isKeyDown(i + GLFW_KEY_1, true)) {
					if (previousOption != selectedOption) {
						setSelectedOption(i);
					} else if (menuOptionSelect != null){
						menuOptionSelect.call(i);
					}
				}
			}
		}
		
		if (MouseInput.isButtonDown(GLFW_MOUSE_BUTTON_1) && !MouseInput.isButtonDown(GLFW_MOUSE_BUTTON_1, true)) {
			int option = clickedOption();
			if (option > -1) {
				setSelectedOption(option);
			}
			if (selectedOption == previousOption) {
				if (menuOptionSelect != null) {
					menuOptionSelect.call(clickedOption());
				}
			}
		}
	}
	
	@Override
	public void Draw(SpriteBatch sb) {
		Clear();
		int scrollOffset = 0;
		if (selectedOption >= 10) {
			scrollOffset = selectedOption - 10;
		}
		for (int i = scrollOffset; i < (spriteMode ? spriteOptions.size() : options.size()) && i < (horizontal ? size.x : size.y) + scrollOffset; i++) {
			Vector2i o = new Vector2i(0, i - scrollOffset);
			if (horizontal) {
				o = new Vector2i(i - scrollOffset, 0);
			}
			setLine(o, i);
		}
		super.Draw(sb);
	}
	
	private void setLine(Vector2i o, int i) {
		if (spriteMode) {
			setSprite(o, spriteOptions.get(i));
		} else {
			setSpriteLine(o, options.get(i), new Vector3f());
		}
		optionColorer.runOn(this, i, o);
	}

	void Clear() {
		setAllSpritesRandom(" ");
	}
	
	public int clickedOption() {
		Vector2f mousePos = new Vector2f();
		MouseInput.getPos(mousePos);
		mousePos.mul(1 / (float)LatticeSuit.fontSize);
		for (int i = 0; i < options.size(); i++) {
			if (mouseIsOverOption(mousePos, i)) {
				return i;
			}
		}
		return -1;
	}
	
	public int getSelectedOption() {
		return selectedOption;
	}
	
	public void setSelectedOption(int i) {
		previousOption = selectedOption;
		selectedOption = i;
	}

	private boolean mouseIsOverOption(Vector2f mousePos, int i) {
		if (!horizontal) {
			return position.y + i == Math.floor(mousePos.y) && mousePos.x >= position.x && mousePos.x < size.x + position.x;
		} else {
			return position.x + i == Math.floor(mousePos.x) && mousePos.y >= position.y && mousePos.y < size.y + position.y;
		}
		
	}
}

class DefaultOptionColorer implements IOptionColorer {
	public Vector3f highlightColor = new Vector3f(0, 1, 0);
	public Vector3f greyedColor = new Vector3f(0.4f, 0.4f, 0.4f);
	public Vector3f defaultColor = new Vector3f(1f, 1f, 1f);
	@Override
	public void runOn(Menu menu, int optionIndex, Vector2i optionPosition) {
		Vector3f colorToUse;
		if (optionIndex == menu.selectedOption) {
			if (menu.selected) {
				colorToUse = highlightColor;
			} else {
				colorToUse = greyedColor;
			}
		} else {
			colorToUse = defaultColor;
		}
		Vector2i menuSize = new Vector2i();
		menu.getSize(menuSize);
		if (menu.horizontal) {
			for (int y = 0; y < menuSize.y; y++) {
				menu.bufColor(optionPosition.x, y, colorToUse);
			}
		} else {
			for (int x = 0; x < menuSize.x; x++) {
				menu.bufColor(x, optionPosition.y, colorToUse);
			}
		}
		menu.pushBuffers();
	}
}
