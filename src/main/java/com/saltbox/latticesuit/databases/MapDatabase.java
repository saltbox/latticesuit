package com.saltbox.latticesuit.databases;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;

import com.opencsv.CSVWriter;
import com.saltbox.latticesuit.ActorSpawn;
import com.saltbox.latticesuit.EntitySpawn;
import com.saltbox.latticesuit.ItemSpawn;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.PlayerSpawn;
import com.saltbox.latticesuit.entities.Map;
import com.saltboxgames.latticelight.logging.Logger;

public class MapDatabase extends Database {

	public MapDatabase() {
		super("Data/Maps");
	}
	
	public Map loadMap(String name) {
		Map m = new Map();
		if (!entries.containsKey(name)) {
			Logger.writeLine(name + " not found");
			return m;
		}
		String[] mapData = entries.get(name).get("0, 0").get("data").split(",");
		for (int x = 0; x < m.getXSize(); x++) {
			for (int y = 0; y < m.getYSize(); y++) {
				m.SetTileType(x, y, Integer.parseInt(mapData[y + (x * m.getYSize())]));
			}
		}
		Object[] spawnDataO = entries.get(name).keySet().toArray();
		String[] spawnData = Arrays.copyOf(spawnDataO, spawnDataO.length, String[].class) ;
		for (int i = 0; i < spawnData.length; i++) {
			if (StringUtils.isNumeric(spawnData[i])) {
				HashMap<String, String> row = entries.get(name).get(spawnData[i]);
				if (row != null) {
					if (row.get("item name") != null) {
						String[] pos = row.get("position").split(",");
						m.spawns.add(new ItemSpawn(new Vector2i(Integer.parseInt(pos[0]), Integer.parseInt(pos[1])), row.get("item name")));
					} 
					
					if (row.get("actor name") != null) {
						String[] pos = row.get("position").split(",");
						if (row.get("actor name").equals("player")) {
							m.spawns.add(new PlayerSpawn(new Vector2i(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]))));
						} else {
							m.spawns.add(new ActorSpawn(new Vector2i(Integer.parseInt(pos[0]), Integer.parseInt(pos[1])), row.get("actor name"), Integer.parseInt(row.get("fireteam"))));
						}
					}
				}
			}
		}
		
		Logger.writeLine("loaded map " + name);
		m.triggerAllSpawns();
		return m;
	}
	
	public void saveMap(Map m, String name) {
		File tiles = new File("Data/Maps/" + name + ".csv");
		try {
			CSVWriter tileWriter = new CSVWriter(new FileWriter(tiles), ';');
			writeTileData(m, tileWriter);
			tileWriter.close();
			
			Logger.writeLine("saved map " + name);
		} catch (IOException e) {
			Logger.writeLine(e.toString());
			e.printStackTrace();
		}
	}

	private void writeTileData(Map m, CSVWriter writer) {
		writer.writeNext(new String[] {"region", "data"});
		String[] mapData = new String[2];
		mapData[0] = "0, 0";
		mapData[1] = "";
		for (int x = 0; x < m.getXSize(); x++) {
			for (int y = 0; y < m.getYSize(); y++) {
				mapData[1] += m.GetTileType(x, y) + ",";
			}
		}
		writer.writeNext(mapData);
		
		int spawnID = 0;
		writer.writeNext(new String[] {"new columns", "item spawns"});
		writer.writeNext(new String[] {"id", "position", "item name"});
		mapData = new String[2];
		for (EntitySpawn spawn : m.spawns) {
			if (spawn.getSpawnType().equals("item")) {
				writer.writeNext(new String[] {Integer.toString(spawnID), spawn.getPosition().x + "," + spawn.getPosition().y,spawn.getName()});
				spawnID++;
			}
		}
		
		writer.writeNext(new String[] {"new columns", "actor spawns"});
		writer.writeNext(new String[] {"id", "position", "actor name", "fireteam"});
		mapData = new String[2];
		for (EntitySpawn spawn : m.spawns) {
			if (spawn.getSpawnType().equals("actor")) {
				ActorSpawn a = (ActorSpawn)spawn;
				writer.writeNext(new String[] {Integer.toString(spawnID), a.getPosition().x + "," + a.getPosition().y,a.getName(), String.valueOf(a.fireteamNum)});
				spawnID++;
			}
		}
		
	}
}
