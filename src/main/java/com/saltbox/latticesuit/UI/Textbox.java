package com.saltbox.latticesuit.UI;

import java.util.ArrayList;

import org.apache.commons.lang3.text.WordUtils;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Textbox extends UIGameObject {
	Vector2i linePos = new Vector2i();
	Vector3f color = new Vector3f(1f, 1f, 1f);
	public ArrayList<String> logText;
	String[] stringsToDraw;
	
	public Textbox(Vector2f position, float zDepth, Vector2i size) {
		super(position, zDepth, size);
		logText = new ArrayList<String>();
		stringsToDraw = new String[size.y];
	}
	
	public Textbox(Vector2f position, float zDepth) {
		this(position, zDepth, new Vector2i(24, 20));
	}
	
	public void updateTextAsLog(String s) {
		logText.add(s);
		
	}

	private void compileStringsToDraw() {
		int lineRepeats = 0;
		String previousLine = "TEXTBOX BUG ALERT THIS LINE SHOULD NEVER BE SEEN";
		int outputIndex = size.y - 1;
		int inputIndex = logText.size() - 1;
		while (outputIndex >= 0 && inputIndex >= 0) {
			if(previousLine.equals(logText.get(inputIndex))) {
				outputIndex++;
				lineRepeats++;
				stringsToDraw[outputIndex] = logText.get(inputIndex) + " x" + (lineRepeats + 1);
			} else {
				stringsToDraw[outputIndex] = logText.get(inputIndex);
				lineRepeats = 0;				
			}
			previousLine = logText.get(inputIndex);
			outputIndex--;
			inputIndex--;
		}
	}
	
	public void updateText(String s) {
		clear();
		linePos.set(0, 0);
		String[] lines = WordUtils.wrap(s, size.x).split("\r\n|\n");
		for(int i = 0; i < lines.length; i++) {
			setSpriteLine(linePos, lines[i], color);
			linePos.y++;
		}
	}
	
	public void clear() {
		setAllSpritesRandom(" ");
	}
	
	@Override
	public void Update(GameTime gameTime) {
		
	}
	
	@Override
	public void Draw(SpriteBatch sb) {
		if (!logText.isEmpty()) {
			for (int i = 0; i < stringsToDraw.length; i++) {
				stringsToDraw[i] = "";
			}
			
			clear();
			compileStringsToDraw();
			
			linePos.set(0, size.y - 1);
			for(int i = stringsToDraw.length - 1; i >= 0; i--) {
				setSpriteLine(linePos, stringsToDraw[i], color);
				linePos.y--;
			}
		}
		
		super.Draw(sb);
	}
}
