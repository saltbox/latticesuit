package com.saltbox.latticesuit;

import org.joml.Vector2f;

import com.saltbox.latticesuit.entities.Entity;

public interface IPlantable extends Entity {
	public void plantFacing(Vector2f direction);
}
