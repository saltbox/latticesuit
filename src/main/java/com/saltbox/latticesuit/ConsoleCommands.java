package com.saltbox.latticesuit;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.databases.ItemDatabase;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.items.Weapon;
import com.saltbox.latticesuit.playercommands.DebugData;
import com.saltboxgames.latticelight.Game;
import com.saltboxgames.latticelight.logging.Logger;

public class ConsoleCommands {
	public static void defineConsoleCommands(Game game, Player player) {
		game.registerCommand("cmode", (String[] args) -> {
			if (args != null) {
				player.activateCommandMode(args[0]);
			} else {
				Logger.writeLine("active command mode is " + player.commandMode);
			}
		});

		game.registerCommand("givePlayer", (String[] args) -> {
			if (args != null) {
				if (args.length == 1) {
					player.inventory.add(DatabaseMediator.items.CreateItem(args[0], player.getPosition()));
				}
			} else {
				Logger.writeLine("giveplayer [itemname]");
			}
		});
		
		game.registerCommand("saveMap", (String[] args) -> {
			if (args != null && args.length == 1) {
				DatabaseMediator.maps.saveMap(InGame.map, args[0]);
			}
		});
		
		game.registerCommand("loadMap", (String[] args) -> {
			if (args != null && args.length == 1) {
				InGame.map = DatabaseMediator.maps.loadMap(args[0]);
			}
		});
		
		game.registerCommand("clearmap", (String[] args) -> {
			InGame.map = new Map();
			InGame.entities.clear();
		});
		
		game.registerCommand("see", (String[] args) -> {
			InGame.map.seeAll();
		});
		
		game.registerCommand("testcolor", (String[] args) -> {
			for (int x = 0; x < InGame.map.getXSize(); x++) {
				for (int y = 0; y < InGame.map.getYSize(); y++) {
					InGame.map.setColor(new Vector2i(x, y), new Vector3f(1f, 0.025f, 0.025f));
				}
			}
		});
		
		game.registerCommand("settiletype", (String[] args) -> {
			if (args != null && args.length == 3) {
				InGame.map.SetTileType(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
			} else {
				Logger.writeLine("settiletype [x] [y] [type]");
			}
		});
		
		game.registerCommand("debug", (String[] args) -> {
			debugEntity();
		});
		
		game.registerCommand("d", (String[] args) -> {
			debugEntity();
		});
	}
	
	private static void debugEntity() {
		Vector2f mousePosition = new Vector2f();
		InGame.map.getMousePosInMap(mousePosition);
		for (Entity e : InGame.entities) {
			if (e.getPosition().equals(mousePosition)) {
				((DebugData)InGame.player.commands.get("debugdata")).debugObject(e, true);
			}
		}
	}
}
