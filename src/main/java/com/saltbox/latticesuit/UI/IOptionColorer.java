package com.saltbox.latticesuit.UI;

import org.joml.Vector2i;

public interface IOptionColorer {
	public void runOn(Menu menu, int optionIndex, Vector2i optionPosition);
}
