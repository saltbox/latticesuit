package com.saltbox.latticesuit.items;

import org.joml.Vector2f;

import com.saltbox.latticesuit.entities.Entity;
import com.saltboxgames.latticelight.GameTime;

public abstract class Grenade extends Entity {
	public boolean active;
	public boolean goneOff;
	public int timer, timerMax = 64;
	
	public Grenade(Vector2f position) {
		super(position);
		setSprite('G');
		health = 10;
		timer = timerMax;
		pickupable = true;
		damageable = true;
	}
	
	@Override 
	public void throwEntity(Vector2f originCell, Vector2f targetPosition) {
		super.throwEntity(originCell, targetPosition);
		activate();
	}

	public void activate() {
		active = true;
		timer = timerMax;
	}
	
	public void goOff() {
		goneOff = true;
	}
	
	public void Update(GameTime gameTime) {
		super.Update(gameTime);
		if (active) {
			timer -= 1;
		}
		if (timer == 0 && !goneOff) {
			goOff();
		}
		if (health <= 0) {
			goOff();
		}
	}
}
