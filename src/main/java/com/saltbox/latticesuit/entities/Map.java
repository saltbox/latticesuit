package com.saltbox.latticesuit.entities;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.AStar;
import com.saltbox.latticesuit.EntitySpawn;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.Raycast.Mode;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.databases.TileDefinition;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.Camera;

public class Map extends Entity {
	public int[] tiles;
	public int[] damageMap;
	public int[] coverMap;
	public int[] movementMap;
	public int[] visionMap;
	public boolean[] seenMap;
	public boolean[] seenThisTurnMap;
	public boolean seeAll = false;	
	
	Vector2i mapSize;
	int playerVisionRadius = 100;
	ArrayList<Vector2f> sightVectors;
	
	Vector3f visibleTileColor = new Vector3f(1f, 1f, 1f);
	Vector3f fogOfWarColor = new Vector3f(0.5f, 0.5f, 0.5f);
	
	public Vector2f playerPosLastTurn = null;
	
	public ArrayList<EntitySpawn> spawns = new ArrayList<EntitySpawn>();
	
	

	public Map() {
		super(new Vector2f(0, 0), -1f, new Vector2i(128, 128));
		mapSize = new Vector2i(128, 128);
		tiles = new int[size.x * size.y];
		damageMap = new int[size.x * size.y];
		coverMap = new int[size.x * size.y];
		movementMap = new int[size.x * size.y];
		visionMap = new int[size.x * size.y];
		seenMap = new boolean[size.x * size.y];
		seenThisTurnMap = new boolean[size.x * size.y];
		name = "map";
		seeAll();
		Raycast.map = this;
	}

	@Override
	public void Update(GameTime gameTime) {
		if (seeAll) {
			for (int i = 0; i < getXSize() * getYSize(); i++) {
				seenThisTurnMap[i] = true;
			}
		}
		if (playerPosLastTurn == null) {
			playerPosLastTurn = new Vector2f();
		}
		playerPosLastTurn.set(InGame.player.getPosition());
	}

	private void showIfEntitiesCanSeePlayer() {
		RaycastHit hit;
		for (Entity e : InGame.entities) {
			hit = Raycast.raycast(InGame.player.getPosition(), e.getPosition(), null, InGame.player, Mode.VISION);
			if (hit != null && hit.hitEntity != null && hit.hitEntity.equals(e)) {
				e.setSprite('X');
			} else {
				e.setSprite('O');
			}
		}
	}
	
	public void triggerAllSpawns() {
		for (EntitySpawn spawn : spawns) {
			spawn.spawn();
		}
	}
	
	public int[] getMovementMap() {
		return movementMap;
	}
	
	public int getHealthAt(Vector2f pos) {
		return damageMap[(int) (pos.x + pos.y * size.x)];
	}

	public int GetTileType(Vector2i v) {
		return GetTileType(v.x, v.y);
	}

	public int GetTileType(int x, int y) {
		return tiles[x + (y * size.x)];
	}

	public void SetTileType(int x, int y, int type) {
		if (tileIsInBounds(x, y)) {
			TileDefinition tileDefinition = DatabaseMediator.tiles.getTileDefinition(type);
			tiles[x + (y * size.x)] = type;
			damageMap[x + (y * size.x)] = tileDefinition.getHealth();
			coverMap[x + (y * size.x)] = tileDefinition.getCoverStatus();
			movementMap[x + (y * size.x)] = tileDefinition.getBlockingStatus();
			visionMap[x + (y * size.x)] = tileDefinition.getVisionStatus();
			damageMap[x + (y * size.x)] = tileDefinition.getHealth();
			if (isTileSeen(new Vector2f(x, y))) {
				setSprite(x, y, tileDefinition);
			}
		} else {
			Logger.writeLine("tried to set an invalid tile position");
		}
	}
	
	public void setAllTiles(int type) {
		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {
				SetTileType(x, y, type);
			}
		}
	}
	
	public boolean isTileSeen(Vector2f pos) {
		return seenMap[(int) (pos.x + (pos.y * getXSize()))];
	}
	
	public boolean isTileSeenThisTurn(Vector2f pos) {
		return seenThisTurnMap[(int) (pos.x + (pos.y * getXSize()))];
	}
	
	public void seeTile(Vector2f pos) {
		if (!isTileSeen(pos)) {
			seenMap[(int) (pos.x + (pos.y * size.x))] = true;
			seenThisTurnMap[(int) (pos.x + (pos.y * size.x))] = true;
			setSprite(Math.round(pos.x), Math.round(pos.y), DatabaseMediator.tiles.getTileDefinition(tiles[(int) (pos.x + (pos.y * size.x))]));
			setColor(new Vector2i(Math.round(pos.x), Math.round(pos.y)), visibleTileColor);
		}
	}
	
	public void unseeTile(Vector2f pos) {
		if (!isTileSeen(pos)) {
			seenMap[(int) (pos.x + (pos.y * size.x))] = false;
			setSprite(new Vector2i((int)pos.x, (int)pos.y), ' ');
		}
	}
	
	
	public void bufSeeTile(Vector2f pos) {
		if (seenThisTurnMap[(int) (pos.x + (pos.y * size.x))]) {
			bufColor(Math.round(pos.x), Math.round(pos.y), visibleTileColor);
		}
		if (!isTileSeen(pos)) {
			bufSprite(Math.round(pos.x), Math.round(pos.y), DatabaseMediator.tiles.getTileDefinition(tiles[(int) (pos.x + (pos.y * size.x))]));
			seenMap[(int) (pos.x + (pos.y * size.x))] = true;
		}
	}
	
	public void bufUnseeTile(Vector2f pos) {
		if (!isTileSeen(pos)) {
			seenMap[(int) (pos.x + (pos.y * size.x))] = false;
			bufSprite(new Vector2i((int)pos.x, (int)pos.y), ' ');
		}
	}
	
	void seeTiles(ArrayList<Vector2f> tiles) {
		for (Vector2f pos : tiles) {
			if (pos.x >= 0 && pos.y >= 0 && pos.x < size.x && pos.y < size.y) {
				seeTile(pos);
			}
		}
	}
	
	void bufSeeTiles(ArrayList<Vector2f> tiles) {
		for (Vector2f pos : tiles) {
			if (pos.x >= 0 && pos.y >= 0 && pos.x < size.x && pos.y < size.y) {
				bufSeeTile(pos);
			}
		}
	}
	
	Vector2i temp = new Vector2i();
	public void setColor(Vector2f pos, Vector3f color) {
		temp.set((int)pos.x, (int)pos.y);
		setColor(temp, color);
	}

	private void setSprite(int x, int y, TileDefinition definition) {
		setSprite(new Vector2i(x, y), definition.getSprites()[GameObject.spriteRandom.nextInt(definition.getSprites().length)]);
	}
	
	private void bufSprite(int x, int y, TileDefinition definition) {
		bufSprite(new Vector2i(x, y), definition.getSprites()[GameObject.spriteRandom.nextInt(definition.getSprites().length)]);
	}

	public void SetTileType(Vector2i v, int type) {
		SetTileType(v.x, v.y, type);
	}

	public boolean canMoveInto(int x, int y, boolean tunneling) {
		int index = x + y * size.x;
		if (x >= 0 && y >= 0 && x < size.x && y < size.y) {
			return movementMap[index] == 0 || tunneling;
		} else {
			return false;
		}
	}
	
	public boolean tileIsInBounds(float x, float y) {
		return (x >= 0 && y >= 0 && x < size.x && y < size.y);
	}
	
	public boolean canMoveInto(Vector2f pos) {
		return canMoveInto(pos, false);
	}
	
	public boolean canMoveInto(Vector2f pos, boolean tunneling) {
		return canMoveInto((int)pos.x, (int)pos.y, tunneling);
	}
	
	

	@Override
	public void Damage(int damage, Vector2f damagePosition) {
		if (tileIsInBounds(damagePosition.x, damagePosition.y)) {
			int index = (int) (damagePosition.x + damagePosition.y * size.x);
			damageMap[index] -= damage;
			if (damageMap[index] <= 0) {
				SetTileType((int)damagePosition.x, (int)damagePosition.y, 0);
			}
			if (tiles[index] == 0) {
				InGame.entities.add(new SandSplash((new Vector2f(damagePosition.x, damagePosition.y))));
			}
		}
	}

	Vector2f mousePos = new Vector2f();
	Vector2f cameraPos = new Vector2f();

	public void getMousePosInMap(Vector2i out) {
		MouseInput.getPos(mousePos);
		Camera.Main.getPosition(cameraPos);
		out.set((int) (mousePos.x / LatticeSuit.fontSize + cameraPos.x), (int) (mousePos.y / LatticeSuit.fontSize + cameraPos.y));
	}
	
	Vector2i tempOut = new Vector2i();
	public void getMousePosInMap(Vector2f out) {
		getMousePosInMap(tempOut);
		out.x = tempOut.x;
		out.y = tempOut.y;
	}
	
	public ArrayList<Vector2f> getPath(Vector2f origin, Vector2f destination) {
		return AStar.FindPath(origin, destination);
	}
	
	public int getXSize() {
		return size.x;
	}
	
	public int getYSize() {
		return size.y;
	}
	
	public ArrayList<Vector2f> getPathableNeighbors(Vector2f cell) {
		return getPathableNeighbors(cell, false);
	}
	
	public ArrayList<Vector2f> getPathableNeighbors(Vector2f cell, boolean tunneling) {
		ArrayList<Vector2f> neighbors = new ArrayList<Vector2f>();
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (!(x == 0 && y == 0)) {
					if (canMoveInto(new Vector2f(x, y).add(cell), tunneling)) {
						neighbors.add(new Vector2f(x, y).add(cell));
					}
				}
			}
		}
		return neighbors;
	}
	
	public int getMoveToCellCost(Vector2f origin, Vector2f destination) {
		return Math.round(origin.distance(destination) * Actor.BASE_MOVE_FRAMES);
	}
	
	public int getAStarPathCost(Vector2f origin, Vector2f destination) {
		return Math.round(origin.distance(destination) * Actor.BASE_MOVE_FRAMES) + getHealthAt(destination);
	}
	
	public void seeAll() {
		Vector2f pos = new Vector2f();
		for (int x = 0; x < getXSize(); x++) {
			for (int y = 0; y < getYSize(); y++) {
				pos.x = x;
				pos.y = y;
				seeTile(pos);
			}
		}
		seeAll = true;
	}
	
	public void unseeAll() {
		Vector2f pos = new Vector2f();
		for (int x = 0; x < getXSize(); x++) {
			for (int y = 0; y < getYSize(); y++) {
				pos.x = x;
				pos.y = y;
				bufUnseeTile(pos);
			}
		}
		pushBuffers();
	}
	
	public Vector2i getSize() {
		return mapSize;
	}
}