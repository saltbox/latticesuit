package com.saltbox.latticesuit.playercommands;

import org.joml.Vector2f;

import com.saltbox.latticesuit.Directions;
import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.IMenuOptionSelect;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.UI.PlayerUI;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

public class Get extends PlayerCommand {
	Menu itemsOnGroundMenu;
	ArrayList<Entity> itemsOnGround;
	
	public Get(Player player) {
		super(player);
		itemsOnGroundMenu = new Menu();
		Vector2f menuSize = new Vector2f();
		itemsOnGroundMenu.getSize(menuSize);
		itemsOnGroundMenu.setPosition(new Vector2f(0, LatticeSuit.GetSize().y - (PlayerUI.weaponLines + menuSize.y)));
		itemsOnGroundMenu.menuOptionSelect = new IMenuOptionSelect() {
			
			@Override
			public void call(int option) {
				if (!itemsOnGround.isEmpty()) {
					pickupSelectedItem();
					if (itemsOnGround.isEmpty()) {
						player.activateCommandMode("default");
					}
				}
				
			}
		};
	}

	@Override
	public void draw(SpriteBatch sb) {
		itemsOnGroundMenu.Draw(sb);
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
		}
		itemsOnGroundMenu.Update(gameTime);
	}
	
	@Override
	public void activate() {
		updateObjectsOnGround();
		itemsOnGroundMenu.setSelectedOption(0);
	}
	
	private void updateObjectsOnGround() {
		itemsOnGroundMenu.options.clear();
		
		itemsOnGround = InGame.getAllPickupableEntitiesAtPosition(player.getPosition());
		for (Vector2f cell : Directions.getAll()) {
			itemsOnGround.addAll(InGame.getAllPickupableEntitiesAtPosition(new Vector2f(player.getPosition()).add(cell)));
		}
		for (Entity entity : itemsOnGround) {
			itemsOnGroundMenu.options.add(entity.getName());
		}
	}
	
	private void pickupSelectedItem() {
		Entity item = itemsOnGround.get(itemsOnGroundMenu.getSelectedOption());
		player.addToInventory(item);
		InGame.removeEntityFromList(item);
		itemsOnGround.remove(item);
		updateObjectsOnGround();
		itemsOnGroundMenu.setSelectedOption(Math.min(itemsOnGroundMenu.getSelectedOption(), itemsOnGroundMenu.options.size() - 1));
	}

}
