package com.saltbox.latticesuit;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_1;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_2;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_3;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_4;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_6;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_7;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_8;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_9;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.joml.Vector2f;

public class Directions {
	public static HashMap<Integer, Vector2f> map;

	public static void Set() {
		map = new HashMap<Integer, Vector2f>();
		map.put(GLFW_KEY_KP_8, new Vector2f(0, -1));
		map.put(GLFW_KEY_KP_6, new Vector2f(1, 0));
		map.put(GLFW_KEY_KP_2, new Vector2f(0, 1));
		map.put(GLFW_KEY_KP_4, new Vector2f(-1, 0));

		map.put(GLFW_KEY_KP_9, new Vector2f(1, -1));
		map.put(GLFW_KEY_KP_3, new Vector2f(1, 1));
		map.put(GLFW_KEY_KP_1, new Vector2f(-1, 1));
		map.put(GLFW_KEY_KP_7, new Vector2f(-1, -1));
	}

	public static Vector2f Get(Integer key) {
		return map.get(key);
	}
	
	public static Collection<Vector2f> getAll() {
		return map.values();
	}
}
