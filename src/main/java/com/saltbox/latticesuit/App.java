package com.saltbox.latticesuit;

import com.saltboxgames.latticelight.Game;

public class App {
	public static void main(String[] args) {
		Game game = new LatticeSuit();
		game.Begin();
	}
}
