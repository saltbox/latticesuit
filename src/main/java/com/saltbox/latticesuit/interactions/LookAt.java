package com.saltbox.latticesuit.interactions;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.entities.Entity;

public class LookAt extends Interaction {
	Entity e;
	public LookAt(Entity e) {
		super("look at");
		this.e = e;
	}

	@Override
	public void doInteraction() {
		GameLog.writeLine(e.getDescription());
	}

	@Override
	public int getFrames() {
		return 0;
	}

}
