package com.saltbox.latticesuit;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.entities.Entity;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class CommandTile extends Entity {
	public boolean active;

	public CommandTile(Vector2f position, char character, Vector3f color) {
		super(position, 5);
		setSprite(character);
		setColor(new Vector2i(0, 0), color);
	}

	public CommandTile(Vector2f position) {
		super(position, 5);
	}

	@Override
	public void Update(GameTime gameTime) {

	}

	@Override
	public void Draw(SpriteBatch sb) {
		if (active) {
			super.Draw(sb);
		}
	}

}
