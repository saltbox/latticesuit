package com.saltbox.latticesuit.items;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.SmokeCloud;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;

public class SmokeGrenade extends Grenade {
	int remainingSmoke = 400;
	float smokeProductionRate = 0.25f;
	float smokeTicker = 0f;
	public SmokeGrenade(Vector2f position) {
		super(position);
		name = "smoke grenade";
	}
	
	@Override 
	public void Update(GameTime gt) {
		super.Update(gt);
		if (goneOff && remainingSmoke > 0) {
			smokeTicker += smokeProductionRate;
			while(smokeTicker >= 1f) {
				smokeTicker -= 1f;
				InGame.addEntityToList(new SmokeCloud(getPosition()));
			}
		}
	}
	
	@Override
	public void Damage(int damage) {
		super.Damage(damage);
		if (health <= 0) {
			goOff();
			smokeProductionRate = 2;
		}
	}
	
	@Override
	public String getName() {
		if (remainingSmoke <= 0) {
			return name + " (spent)";
		}
		if (active){
			return name + " (active)";
		}
		return name;
			
	}
}