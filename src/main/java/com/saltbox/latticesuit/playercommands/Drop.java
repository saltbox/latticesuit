package com.saltbox.latticesuit.playercommands;

import com.saltbox.latticesuit.entities.actors.Player;

public class Drop extends InventoryManagementCommand {

	public Drop(Player player) {
		super(player);
		addMenu(player.inventory);
		addMenu(player.equipment);
	}

	@Override
	public void confirmSelection() {
		player.dropItem(selectedInventory, selectedRow);
		super.confirmSelection();
	}
}
