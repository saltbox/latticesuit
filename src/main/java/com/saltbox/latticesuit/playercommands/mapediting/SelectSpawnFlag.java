package com.saltbox.latticesuit.playercommands.mapediting;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.IMenuOptionSelect;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.playercommands.PlayerCommand;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.joml.Vector2f;

public class SelectSpawnFlag extends PlayerCommand {
	Menu actorsMenu;
	Menu itemsMenu;
	ArrayList<String> actorKeys;
	ArrayList<String> itemKeys;
	int selectedMenu = 0;
	
	
	public SelectSpawnFlag(Player player) {
		super(player);
		actorsMenu = new Menu(new Vector2f(0, 1));
		Vector2f actorsMenuSize = new Vector2f();
		actorsMenu.getSize(actorsMenuSize);
		actorKeys = DatabaseMediator.actors.getAllKeys();
		for (String s : actorKeys) {
			actorsMenu.options.add(s);
		}
		
		itemKeys = DatabaseMediator.items.getAllKeys();
		itemsMenu = new Menu(new Vector2f(actorsMenuSize.x, 0));
		for(String s : itemKeys) {
			itemsMenu.options.add(s);
		}
		
		actorsMenu.menuOptionSelect = new IMenuOptionSelect() {
			@Override
			public void call(int option) {
				((EditMap)player.commands.get("editmap")).entityToSpawn = actorKeys.get(option);
				((EditMap)player.commands.get("editmap")).spawnItem = false;
				player.activateCommandMode("editmap");
			}
		};
		
		itemsMenu.menuOptionSelect = new IMenuOptionSelect() {
			@Override
			public void call(int option) {
				((EditMap)player.commands.get("editmap")).entityToSpawn = itemKeys.get(option);
				((EditMap)player.commands.get("editmap")).spawnItem = true;
				player.activateCommandMode("editmap");
				
			}
		};
	}

	@Override
	public void draw(SpriteBatch sb) {
		actorsMenu.Draw(sb);
		itemsMenu.Draw(sb);
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		if (selectedMenu == 0) {
			actorsMenu.Update(gameTime);
		} else {
			itemsMenu.Update(gameTime);
		}
		if (KeyboardInputTools.left()) {
			selectedMenu = 0;
		}
		if (KeyboardInputTools.right()) {
			selectedMenu = 1;
		}
	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub
		
	}

}
