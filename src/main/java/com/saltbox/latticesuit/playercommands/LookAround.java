package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector2f;

import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class LookAround extends PlayerCommand {

	Vector2f lookPosition = new Vector2f();
	Vector2f adjustedLookPosition = new Vector2f();
	public LookAround(Player player) {
		super(player);
	}

	@Override
	public void draw(SpriteBatch sb) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		if (MouseInput.isButtonDown(1)) {
			Vector2f mouseInput = new Vector2f();
			player.getMouseInputDirection(mouseInput);
			lookPosition.add(mouseInput);
		}
		lookPosition.add(player.directionInput);
		Camera.Main.setPosition(lookPosition);
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
			player.cameraLock = true;
		}
		if (!KeyboardInput.isKeyDown(GLFW_KEY_LEFT_SHIFT) && !KeyboardInput.isKeyDown(GLFW_KEY_RIGHT_SHIFT)) {
			player.inputPause(0.03f);
		} else {
			player.inputPause(0.002f);
		}
	}

	@Override
	public void activate() {
		player.cameraLock = false;
		Camera.Main.getPosition(lookPosition);
	}

}
