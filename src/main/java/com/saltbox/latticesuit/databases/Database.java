package com.saltbox.latticesuit.databases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;

public class Database {
	String definitionPath;
	//				  filename		  row             column  cell
	protected HashMap<String, HashMap<String, HashMap<String, String>>> entries;
	protected HashMap<String, HashMap<String, String>> firstEntry;
	
	public Database(String definitionPath) {
		this.definitionPath = definitionPath;
		tryLoadDefinitions();
	}

	public void tryLoadDefinitions() {
		entries = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		File definitionFile = new File(definitionPath);
		if (definitionFile.isDirectory()) {
			for (File f : definitionFile.listFiles()) {
				if (f.getName().endsWith(".csv")) {
					loadDefinitionFile(f);
				}
			}
		} else {
			loadDefinitionFile(definitionFile);
		}
	}
	
	public ArrayList<String> getAllKeys() {
		ArrayList<String> keys = new ArrayList<String>();
		for (String set : entries.keySet()) {
			for (String key : entries.get(set).keySet()) {
				keys.add(key);
			}
		}
		return keys;
	}
	
	boolean firstFile = true;
	public void loadDefinitionFile(File f)
	{
		try {
			CSVReader reader = new CSVReader(new FileReader(f), 0, new CSVParser(';'));
			String[] columns = reader.readNext();
			String splitFilename = f.getName().split("\\.")[0];
			entries.put(splitFilename, new HashMap<String, HashMap<String, String>>());
			firstEntry = new HashMap<String, HashMap<String, String>>();
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				if (nextLine[0].toLowerCase().equals("new columns")) {
					columns = reader.readNext();
					continue;
				}
				entries.get(splitFilename).put(nextLine[0].toLowerCase(), new HashMap<String, String>());
				for (int i = 0; i < columns.length; i++) {
					entries.get(splitFilename).get(nextLine[0].toLowerCase()).put(columns[i], nextLine[i]);
				}
			}
			if (firstFile) {
				firstEntry = entries.get(splitFilename);
			}
			firstFile = false;
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	class EntryNotFoundException extends Exception {
		public EntryNotFoundException(String message) {
			super(message);
		}

		public EntryNotFoundException(String message, Throwable throwable) {
			super(message, throwable);
		}

		private static final long serialVersionUID = 2347377426446857255L;

	}
}
