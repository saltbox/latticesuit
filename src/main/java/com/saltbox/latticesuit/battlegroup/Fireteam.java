package com.saltbox.latticesuit.battlegroup;

import java.util.ArrayList;
import java.util.HashMap;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.AIStates.Assault;
import com.saltbox.latticesuit.AIStates.Idle;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.gamestates.InGame;

public class Fireteam {
	static HashMap<Integer, Fireteam> fireteams = new HashMap<Integer, Fireteam>();
	
	public ArrayList<IFireteamMember> members = new ArrayList<IFireteamMember>();
	public boolean alive = true;
	Vector2f targetLastSeenPosition = null;
	Entity targetEntity = null;
	
	public static void AddMemberToFireteam(IFireteamMember member, int team) {
		if (!fireteams.containsKey(team)) {
			fireteams.put(team, new Fireteam());
		}
		fireteams.get(team).members.add(member);
	}
	
	public static Fireteam getFireteam(int team) {
		return fireteams.get(team);
	}
	
	public Fireteam() {
		targetEntity = InGame.player;
	}
	
	
	boolean isAlive() {
		alive = false;
		for (IFireteamMember member : members) {
			if (member.isAlive()) {
				alive = true;
			}
		}
		return alive;
	}
	
	Vector2f getTargetLastSeenPosition() {
		return targetLastSeenPosition;
	}
	
	Entity getTargetEntity() {
		return targetEntity;
	}
	
	public void updateTargetPosition(Vector2f newPosition) {
		targetLastSeenPosition = new Vector2f(newPosition);
		for(IFireteamMember member : members) {
			member.setTargetPosition(newPosition, false);
			if (Idle.class.isInstance(member.getActiveState())) {
				member.setState("assault");
			}
		}
	}
}
