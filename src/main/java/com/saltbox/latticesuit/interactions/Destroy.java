package com.saltbox.latticesuit.interactions;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.entities.Entity;

public class Destroy extends Interaction {
	Entity e;
	public Destroy(Entity e) {
		super("destroy");
		this.e = e;
	}

	@Override
	public void doInteraction() {
		e.Damage(e.health);
		GameLog.writeLine("You destroy the " + e.getName());
	}

	@Override
	public int getFrames() {
		return 5;
	}

}
