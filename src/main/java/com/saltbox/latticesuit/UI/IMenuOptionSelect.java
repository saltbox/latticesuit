package com.saltbox.latticesuit.UI;

public interface IMenuOptionSelect {
	public void call(int option);
}
