package com.saltbox.latticesuit.AIStates;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.battlegroup.IFireteamMember;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.entities.actors.AIActor;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;

public class Assault extends AIState {
	ArrayList<Vector2f> pathToTarget;
	static Random rng = new Random();
	
	public Assault(AIActor actor) {
		super(actor);
	}

	@Override
	public void Start() {
		
	}

	@Override
	public void Update(GameTime gt) {
		actor.lookOutForPlayer();
		if (actor.targetPosition == null || actor.targetPosition.equals(actor.getPosition())) {
			stopPursuit();
			return;
		} 
		
		if (actor.canSeeTarget()) {
			if (actor.noAlliesInFrontOfTarget()) {
				actor.setState("firefight");
			} else {
				actor.setState("firefightreposition");
			}
		} else {
			if (!pathExists() || !pathIsCorrect()) {
				repathToTarget();
			}
			if (!pathToTarget.isEmpty()) {
				followPath();
			} else {
				stopPursuit();
			}
		}
	}

	private void followPath() {
		if (cellHasSquadmate(pathToTarget.get(0))) {
			IFireteamMember squadmate;
			squadmate = getSquadmateAtCell(pathToTarget.get(0));
			if (squadmate != null) {
				if (Firefight.class.isInstance(squadmate.getActiveState())) {
					actor.setState("firefightreposition");
				} else {
					if (rng.nextBoolean()) {
						actor.Move(pathToTarget.remove(0).sub(actor.getPosition()));
					}
				}
			}
		} else {
			actor.Move(pathToTarget.remove(0).sub(actor.getPosition()));
		}
	}

	private void stopPursuit() {
		actor.setState("idle");
		pathToTarget = null;
		actor.targetPosition = null;
		
	}

	private void repathToTarget() {
		pathToTarget = InGame.map.getPath(actor.getPosition(), actor.targetPosition);
	}

	private boolean pathIsCorrect() {
		return actor.targetPosition.equals(pathToTarget.get(pathToTarget.size() - 1));
	}

	private boolean pathExists() {
		return pathToTarget != null && !pathToTarget.isEmpty();
	}
	
	private boolean cellHasSquadmate(Vector2f cell) {
		boolean b = false;
		for (IFireteamMember squadmate : actor.getFireteam().members) {
			if (squadmate != actor && squadmate.getPosition().equals(cell)) {
				b = true;
			}
		}
		return b;
	}
	
	private IFireteamMember getSquadmateAtCell(Vector2f cell) {
		for (IFireteamMember squadmate : actor.getFireteam().members) {
			if (squadmate != actor && squadmate.getPosition().equals(cell)) {
				return squadmate;
			}
		}
		return null;
	}
}
