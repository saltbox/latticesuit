package com.saltbox.latticesuit.UI;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.gamestates.InGame;

public class ShotDisplayer {
	int shotsPerShooter = 40;
	int maxShots = 1000;
	int maxPathAge = 50;
	
	ArrayList<ShotPath> shots = new ArrayList<ShotPath>();
	HashMap<Entity, Integer> shooters = new HashMap<Entity, Integer>();
	Vector3f colorNew = new Vector3f(1, 0, 0);
	Vector3f colorOld = new Vector3f(1, 1, 1);
	Vector3f colorInterp = new Vector3f();
	
	public void addNewShot(ArrayList<Vector2f> cells, Entity shooter) {
		shots.add(0, new ShotPath(cells, shooter));
	}
	
	
	public void Update() {
		for(ShotPath path : shots) {
			path.Update();
		}
	}
	
	public void drawAllShots() {
		for(int i = shots.size() - 1; i >= 0; i--) {
			ShotPath path = shots.get(i);
			if (path.age > maxPathAge) {
				shots.remove(path);
				path.Draw(colorOld);
				i = Math.min(i + 1, shots.size());
			} else {
				float ratio = ((float)path.age / (float)shotsPerShooter);
				colorInterp.set(lerp(colorNew.x, colorOld.x, ratio), lerp(colorNew.y, colorOld.y, ratio), lerp(colorNew.z, colorOld.z, ratio));
				path.Draw(colorInterp);
			}
		}
		
		InGame.map.pushBuffers();
		
		shooters.clear();
		if (shots.size() > maxShots) {
			shots.removeAll(shots.subList(maxShots, (shots.size() - 1)));
		}
	}
	
	float lerp(float start, float end, float percent)
	{
	     return (start + percent*(end - start));
	}
	
	class ShotPath {
		ArrayList<Vector2f> cells;
		public Entity shooter;
		int age;
		
		public ShotPath(ArrayList<Vector2f> cells, Entity shooter) {
			this.cells = new ArrayList<Vector2f>();
			for (Vector2f v : cells) {
				this.cells.add(new Vector2f(v));
			}
			this.shooter = shooter;
		}
		
		public void Draw(Vector3f color) {
			for (Vector2f v : cells) {
				InGame.map.bufColor((int)v.x, (int)v.y, color);
			}
		}
		
		public void Update() {
			age++;
		}
	}
}