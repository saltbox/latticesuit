package com.saltbox.latticesuit.entities;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.interactions.Interaction;
import com.saltbox.latticesuit.interactions.Interactions;
import com.saltbox.latticesuit.interactions.LookAt;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.logging.Logger;

public class Entity extends GameObject {

	public static Random attackRandom = new Random();
	public static final int tilesPerTickWhenThrown = 3;

	public boolean targetable, pickupable, raycastable, damageable;
	
	public boolean thrown, selfDamageOnMelee;
	protected Vector2f throwTarget;
	
	public int health, maxHealth, team, actionFrames, raycastMask;
	public int meleeDamage = 5, meleeFrames = 4;
	
	public ArrayList<Interaction> interactions;
	
	protected String name, description;
	
	public Vector2f peekReturnCell;

	public Entity(Vector2f position, float zDepth, Vector2i size) {
		super(position, zDepth, size);
		targetable = false;
		damageable = false;
		raycastable = false;
		pickupable = true;
		selfDamageOnMelee = true;
		
		thrown = false;
		
		health = 1;
		maxHealth = 1;
		team = 0;
		actionFrames = 0;
		name = "";
		description = "";
		raycastMask = 0;
		meleeDamage = 5;
		
		interactions = new ArrayList<Interaction>();
		
		interactions.addAll(Interactions.baseSet(this));
	}

	public Entity(Vector2f position, float zDepth) {
		this(position, zDepth, new Vector2i(1, 1));
	}

	public Entity(Vector2f position) {
		this(position, 1);
	}

	@Override
	public void Update(GameTime gameTime) {
		if (damageable && health <= 0) {
			InGame.removeEntityFromList(this);
		}
		if (peekReturnCell != null && actionFrames <= 1) {
			setPosition(peekReturnCell);
			peekReturnCell = null;
			onReturnFromPeek();
		}
		if (actionFrames > 0) {
			actionFrames--;
		}
		if (thrown) {
			throwTick();
		}
	}

	public void throwTick() {
		if (getPosition().equals(throwTarget)) {
			thrown = false;
		} else {
			ArrayList<Vector2f> hitCells = new ArrayList<Vector2f>();
			RaycastHit hit = Raycast.raycast(getPosition(), throwTarget, hitCells, this, Raycast.Mode.MOVEMENT.maskIndex | Raycast.Mode.ENEMY.maskIndex);
			if (hitCells.isEmpty()) {
				thrown = false;
				hit.hitEntity.Damage(meleeDamage, hit.hitPosition);
				if (selfDamageOnMelee) {
					Damage(meleeDamage);
				}
			}
			for (int i = 0; i < hitCells.size() && i < Entity.tilesPerTickWhenThrown; i++) {
				if ((hitCells.get(i).equals(throwTarget) || hitCells.get(i).equals(hit.hitPosition))) {
					thrown = false;
					setPosition(hitCells.get(i));
					if (hit.hitEntity != null) {
						hit.hitEntity.Damage(meleeDamage, hitCells.get(i));
						if (selfDamageOnMelee) {
							Damage(meleeDamage);
						}
					} else {
						InGame.map.Damage(meleeDamage, hitCells.get(i));
						if (selfDamageOnMelee) {
							Damage(meleeDamage);
						}
					}
				}
			}
			if (thrown) {
				setPosition(hitCells.get(Math.min(Entity.tilesPerTickWhenThrown, hitCells.size()) - 1));
			}
		}
	}

	static Vector2f zero = new Vector2f();
	static Vector2f playerPosition = new Vector2f();
	
	public boolean Move(Vector2f move) {
		return Move(move, true);
	}

	public boolean Move(Vector2f move, boolean takeFrames) {
		if (move.equals(zero)) {
			return false;
		}
		Vector2f cellToMoveTo = new Vector2f(position).add(move);
		if (InGame.map.canMoveInto(cellToMoveTo)) {
			setPosition(cellToMoveTo);
			if (takeFrames) {
				TakeTurn(InGame.map.getMoveToCellCost(position, new Vector2f(position).add(move)));
			}
		} else {
			return false;
		}
		return true;
	}

	public void TakeTurn(Integer frames) {
		actionFrames = frames;
	}
	
	public void TakeTurnAndPeek(Integer frames, Vector2f peekCell) {
		TakeTurn(frames);
		peekReturnCell = new Vector2f(getPosition());
		setPosition(peekCell);
 	}
	
	protected void onReturnFromPeek() {
		
	}

	public void Damage(int damage) {
		this.Damage(damage, getPosition());
	}

	public void Damage(int damage, Vector2f damagePosition) {
		if (damageable && damage > 0) {
			health -= damage;
		}
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String desc) {
		this.description = desc;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public float getDeflectionChance() {
		return 0.1f;
	}
	
	public void throwEntity(Vector2f originCell, Vector2f targetPosition) {
		InGame.addEntityToList(this);
		setPosition(originCell);
		this.throwTarget = new Vector2f(targetPosition);
		thrown = true;
	}
	
	@Override
	public String toString() {
		String out = "";
		for (Field f : this.getClass().getFields()) {
			out += f.getName() + ": " + f.toString() + "\n";
			
		}
	    return out;
	}
}
