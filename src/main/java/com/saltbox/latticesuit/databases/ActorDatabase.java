package com.saltbox.latticesuit.databases;

import java.util.Arrays;
import java.util.HashMap;

import org.joml.Vector2f;

import com.saltbox.latticesuit.battlegroup.IFireteamMember;
import com.saltbox.latticesuit.entities.actors.*;
import com.saltboxgames.latticelight.logging.Logger;

public class ActorDatabase extends Database {
	public ActorDatabase() {
		super("Data/Definitions/actors.csv");
	}
	
	public String[] getActorNames() {
		Object[] keys = firstEntry.keySet().toArray();
		return Arrays.copyOf(keys, keys.length, String[].class);
	}

	public Actor CreateActor(String name, Vector2f position) {
		HashMap<String, String> stats = firstEntry.get(name);
		Actor actor = null;
		try {
			switch (stats.get("type")) {
				case "soldier":
					actor = new Soldier(position);
					break;
				case "target dummy":
					actor = new TargetDummy(position);
					break;
			}
			if (actor == null) {
				throw new EntryNotFoundException("Actor type" + stats.get("type") + " not found");
			} 
			
			if (!stats.get("items").equals("")) {
				for (String s : stats.get("items").split(",")) {
					if (!s.equals("")) {
						actor.addToInventory(DatabaseMediator.items.CreateItem(s, position));
					}
				}
			}
			if (!stats.get("equipment").equals("")) {
				String[] equipmentStrings = stats.get("equipment").split(","); 
				for (int i = 0; i < equipmentStrings.length; i++) {
					actor.setEquipment(i, DatabaseMediator.items.CreateItem(equipmentStrings[i], position));
				}
			}
		} catch (EntryNotFoundException e) {
			Logger.writeLine(e.toString());
			e.printStackTrace();
		}
		return actor;
	}
}
