package com.saltbox.latticesuit;

import static org.lwjgl.glfw.GLFW.*;

import com.saltboxgames.latticelight.input.KeyboardInput;

public class KeyboardInputTools {
	public static boolean confirm() {
		return (KeyboardInput.isKeyDown(GLFW_KEY_ENTER) && !KeyboardInput.isKeyDown(GLFW_KEY_ENTER, true))  || (KeyboardInput.isKeyDown(GLFW_KEY_KP_ENTER) && !KeyboardInput.isKeyDown(GLFW_KEY_KP_ENTER, true));
	}
	
	public static boolean up() {
		return (KeyboardInput.isKeyDown(GLFW_KEY_KP_8) && !KeyboardInput.isKeyDown(GLFW_KEY_KP_8, true)) || (KeyboardInput.isKeyDown(GLFW_KEY_UP) && !KeyboardInput.isKeyDown(GLFW_KEY_UP, true));
	}
	
	public static boolean down() {
		return (KeyboardInput.isKeyDown(GLFW_KEY_KP_2) && !KeyboardInput.isKeyDown(GLFW_KEY_KP_2, true)) || (KeyboardInput.isKeyDown(GLFW_KEY_DOWN) && !KeyboardInput.isKeyDown(GLFW_KEY_DOWN, true));
	}
	
	public static boolean left() {
		return (KeyboardInput.isKeyDown(GLFW_KEY_KP_4) && !KeyboardInput.isKeyDown(GLFW_KEY_KP_4, true)) || (KeyboardInput.isKeyDown(GLFW_KEY_LEFT) && !KeyboardInput.isKeyDown(GLFW_KEY_LEFT, true));
	}
	
	public static boolean right() {
		return (KeyboardInput.isKeyDown(GLFW_KEY_KP_6) && !KeyboardInput.isKeyDown(GLFW_KEY_KP_6, true)) || (KeyboardInput.isKeyDown(GLFW_KEY_RIGHT) && !KeyboardInput.isKeyDown(GLFW_KEY_RIGHT, true));
	}
}
