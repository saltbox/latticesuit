package com.saltbox.latticesuit.playercommands;

import org.joml.Vector2f;

import com.saltbox.latticesuit.IPlantable;
import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Plant extends PlayerCommand {
	public Entity entityToPlant;
	Vector2f direction = new Vector2f();
	float radius = 1.5f;
	public Plant(Player player) {
		super(player);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(SpriteBatch sb) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}
	
	Vector2f mousePosition = new Vector2f();
	@Override
	public void update(GameTime gameTime) {
		if (MouseInput.isButtonDown(0) && !MouseInput.isButtonDown(0, true)) {
			InGame.map.getMousePosInMap(mousePosition);
			if (mousePosition.distance(player.getPosition()) < radius) {
				player.cursor.setPosition(mousePosition);
				direction.set(new Vector2f(player.cursor.getPosition()).sub(player.getPosition()));
			}
		}
		
		if (KeyboardInputTools.confirm()) {
			player.dropItem(entityToPlant);
			if (entityToPlant instanceof IPlantable) {
				((IPlantable)entityToPlant).plantFacing(direction);
			}
		}
	}

	@Override
	public void activate() {
		player.cursor.active = true;
	}

}
