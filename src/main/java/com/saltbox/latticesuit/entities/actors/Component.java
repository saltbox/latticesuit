package com.saltbox.latticesuit.entities.actors;

public class Component {
	public Component(String name) {
		this(name, 25);
	}
	
	public Component(String name, int health) {
		this(name, health, 0.1f);
	}
	
	public Component(String name, int health, float hitChance) {
		this.name = name;
		this.health = health;
		this.hitChance = hitChance;
	}
	
	public ComponentHit Damage(int damage) {
		
		ComponentHit hit = new ComponentHit();
		if (health <= 0) {
			hit.componentWasAlreadyDestroyed = true;
		}
		health -= damage;
		if (health < 0) {
			hit.remainingDamage = -health;
			hit.componentIsDestroyed = true;
		}
		hit.name = this.name;
		hit.hitComponent = this;
		return hit;
	}
	
	public String name;
	public int health;
	public float hitChance;
}

class ComponentHit {
	public Component hitComponent;
	public String name;
	public int remainingDamage = 0;
	public boolean componentIsDestroyed = false;
	public boolean componentWasAlreadyDestroyed = false;
}

