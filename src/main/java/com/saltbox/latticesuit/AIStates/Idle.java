package com.saltbox.latticesuit.AIStates;

import java.util.Random;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.AIActor;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltboxgames.latticelight.GameTime;

public class Idle extends AIState {
	static Random idleRandom;
	Vector2f idlePoint = new Vector2f();
	int idleStepTime = 75;
	int idleStepTimer = 0;
	public Idle(AIActor actor) {
		super(actor);
	}

	@Override
	public void Start() {
		if (idleRandom == null) {
			idleRandom = new Random();
		}
		actor.getPosition(idlePoint);
	}

	@Override
	public void Update(GameTime gt) {
		idleStepTimer++;
		if (idleStepTimer >= idleStepTime) {
			idleStepTimer = 0;
			if (idleRandom.nextFloat() > 0.75f) {
				moveRandomly();
			}
		}
		actor.lookOutForPlayer();
		if (actor.targetPosition != null) {
			actor.setState("assault");
			actor.TakeTurn(50);
		}
	}
	
	private void moveRandomly() {		
		Vector2f randomVector = new Vector2f(idleRandom.nextInt(3) - 1, idleRandom.nextInt(3) - 1);
		Vector2f targetCell = new Vector2f(randomVector).add(actor.getPosition());
		if (targetCell.distance(idlePoint) <= 2) { 
			actor.Move(randomVector);
		}
	}
}
