package com.saltbox.latticesuit.interactions.weaponinteractions;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.interactions.Interaction;
import com.saltbox.latticesuit.items.Weapon;

public class Reload extends Interaction {
	Weapon weapon;
	public Reload(Weapon w) {
		super("reload");
		this.weapon = w;
	}

	@Override
	public void doInteraction() {
		if (weapon.magStatus < weapon.magSize) {
			weapon.reload();
			GameLog.writeLine("You reload the " + weapon.getName());
			return;
		}
		GameLog.writeLine("The " + weapon.getName() + " is already fully loaded");
	}

	@Override
	public int getFrames() {
		if (weapon.magStatus < weapon.magSize) {
			return weapon.reloadTime;
		}
		return 0;
	}

}
