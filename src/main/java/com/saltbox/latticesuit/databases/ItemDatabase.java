package com.saltbox.latticesuit.databases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.joml.Vector2f;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.items.FragGrenade;
import com.saltbox.latticesuit.items.Grenade;
import com.saltbox.latticesuit.items.SmokeGrenade;
import com.saltbox.latticesuit.items.Weapon;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.logging.Logger;

public class ItemDatabase extends Database{
	HashMap<String, EntityBuilder> builders;
	
	public ItemDatabase() {
		super("Data/Definitions/Items");
		builders = defineBuilders();
	}
	
	public Entity CreateItem(String name, Vector2f position) {
		return CreateItem("", name, position);
	}

	public Entity CreateItem(String itemType, String name, Vector2f position) {
		Entity entity = null;
		try {
			if (itemType == "") {
				for (EntityBuilder builder : builders.values()) {
					if (builder.contains(name)) {
						entity = builder.build(name, position);
						break;
					}
				}
			} else {
				entity = builders.get(itemType.toLowerCase()).build(name, position);
			}
			
			if (entity == null) {
				throw new EntryNotFoundException("Item " + name.toLowerCase() + " not found");
			}
		} catch (EntryNotFoundException e) {
			e.printStackTrace();
			Logger.writeLine(e.toString());
		}
		return entity;
	}
	
	interface EntityBuilder {
		public Entity build(String name, Vector2f position) throws EntryNotFoundException;
		public boolean contains(String name);
	}
	
	public HashMap<String, EntityBuilder> defineBuilders() {
		HashMap<String, EntityBuilder> builders = new HashMap<String, EntityBuilder>();
		
		builders.put("grenade", new EntityBuilder() {
			@Override
			public Grenade build(String name, Vector2f position) throws EntryNotFoundException {
				Grenade g = null;
				switch(name) {
					case "smoke grenade": g = new SmokeGrenade(position);
						break;
					case "frag grenade": g = new FragGrenade(position);
						break;
				}
				g.setName(name);
				return g;
			}

			@Override
			public boolean contains(String name) {
				return entries.get("grenade").get(name.toLowerCase()) != null;
			}
		});
		
		builders.put("item", new EntityBuilder() {
			@Override
			public Entity build(String name, Vector2f position) throws EntryNotFoundException {
				Entity e = new Entity(position);
				HashMap<String, String> stats = null;
				stats = entries.get("item").get(name.toLowerCase());
				if (stats == null) {
					throw new EntryNotFoundException("Item " + name + " of type Weapon not found");
				}
				
				e.setName(stats.get("name").toLowerCase());
				e.setDescription(stats.get("description").toLowerCase());
				e.maxHealth = Integer.parseInt(stats.get("health"));
				e.health = e.maxHealth;
				e.meleeDamage = Integer.parseInt(stats.get("meleedamage"));
				e.meleeFrames = Integer.parseInt(stats.get("meleeframes"));
				e.selfDamageOnMelee = Boolean.parseBoolean(stats.get("selfdamageonmelee"));
				e.setSprite('I');
				return e;
			}

			@Override
			public boolean contains(String name) {
				return entries.get("item").get(name.toLowerCase()) != null;
			}
			
		});
		
		builders.put("weapon", new EntityBuilder() {
			@Override
			public Weapon build(String name, Vector2f position) throws EntryNotFoundException {
				Weapon w = new Weapon(position);
				HashMap<String, String> stats = null;
				stats = entries.get("weapon").get(name.toLowerCase());
				if (stats == null) {
					throw new EntryNotFoundException("Item " + name + " of type Weapon not found");
				}

				w.setName(stats.get("name").toLowerCase());
				w.setDescription(stats.get("description"));
				w.useTime = Integer.parseInt(stats.get("firetime"));
				w.magSize = Integer.parseInt(stats.get("magsize"));
				w.magStatus = w.magSize;
				w.damage = Integer.parseInt(stats.get("damage"));
				w.shotsPerAttack = Integer.parseInt(stats.get("shotsperattack"));
				w.projectilesPerShot = Integer.parseInt(stats.get("projectilespershot"));
				w.health = Integer.parseInt(stats.get("health"));
				w.maxHealth = w.health;
				w.reloadTime = Integer.parseInt(stats.get("reloadtime"));
				w.accuracy = Float.parseFloat(stats.get("accuracy"));
				w.internalsTestTime = Integer.parseInt(stats.get("internalstesttime"));
				w.fieldRepairTime = Integer.parseInt(stats.get("fieldrepairtime"));
				w.meleeDamage = Integer.parseInt(stats.get("meleedamage"));
				w.meleeFrames = Integer.parseInt(stats.get("meleeframes"));
				w.selfDamageOnMelee = Boolean.parseBoolean(stats.get("selfdamageonmelee"));
				return w;
			}

			@Override
			public boolean contains(String name) {
				// TODO Auto-generated method stub
				return entries.get("weapon").get(name.toLowerCase()) != null;
			}
		});
		
		return builders;
	}
}
