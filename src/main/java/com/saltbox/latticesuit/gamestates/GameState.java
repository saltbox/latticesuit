package com.saltbox.latticesuit.gamestates;

import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public abstract class GameState {
	public abstract void Start();
	public abstract void Update(GameTime gameTime);
	public abstract void Draw(SpriteBatch spriteBatch);
}
