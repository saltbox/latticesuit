package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;
import java.util.Collections;

import javax.sql.PooledConnection;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.SandSplash;
import com.saltbox.latticesuit.entities.TilePool;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Sprint extends PlayerCommand {
	public ArrayList<Vector2f> tentativeSprintList = new ArrayList<Vector2f>();
	public ArrayList<Vector2f> committedSprintList = new ArrayList<Vector2f>();
	public TilePool sprintTiles;

	int tilesSprinted = 0;
	boolean sprinting = false;

	public Sprint(Player player) {
		super(player);
		name = "sprint";
		sprintTiles = new TilePool('X', new Vector3f(0, 1f, 0.2f));
	}

	@Override
	public void draw(SpriteBatch sb) {
		if (tentativeSprintList.size() > 0) {
			drawTileList(sb, tentativeSprintList);
		} else if (committedSprintList.size() > 0) {
			drawTileList(sb, committedSprintList);
		}
	}

	private void drawTileList(SpriteBatch sb, ArrayList<Vector2f> sprintList) {
		for (int i = 0; i < sprintList.size(); i++) {
			sprintTiles.get(i).active = true;
			sprintTiles.get(i).setPosition(sprintList.get(i));
		}
		sprintTiles.Draw(sb);
	}

	@Override
	public boolean doCommand() {
		if (committedSprintList != null && !committedSprintList.isEmpty()) {
			int moveFrames = InGame.map.getMoveToCellCost(player.getPosition(), new Vector2f(committedSprintList.get(0)));
			player.Move(committedSprintList.get(0).sub(player.getPosition()), false);
			committedSprintList.remove(0);
			sprintTiles.get(0).active = false;
			if (tilesSprinted >= 1) {
				player.TakeTurn(player.sprintFrames);
				InGame.entities.add(new SandSplash(player.getPosition()));
			} else {
				player.TakeTurn(moveFrames);
			}
			tilesSprinted++;
			if (committedSprintList.isEmpty()) {
				sprintTiles.hideAll();
				player.cameraLock = false;
				sprinting = false;
			}
			return true;
		}
		return false;
	}

	Vector2f zero = new Vector2f();
	Vector2f cursorPosition = new Vector2f();

	@Override
	public void update(GameTime gameTime) {
		if (!sprinting) {
			if (MouseInput.isButtonDown(0) && !MouseInput.isButtonDown(0, true)) {
				Vector2f mousePosition = new Vector2f();
				InGame.map.getMousePosInMap(mousePosition);
				player.cursor.setPosition(mousePosition);
				Camera.Main.setPosition(new Vector2f(player.cursor.getPosition()).sub(LatticeSuit.halfSize));
				sprintTiles.hideAll();
				if (KeyboardInput.isKeyDown(GLFW_KEY_LEFT_SHIFT)) {
					ArrayList<Vector2f> additionalPath = InGame.map.getPath(tentativeSprintList.isEmpty() ? player.getPosition() : tentativeSprintList.get(tentativeSprintList.size() - 1), player.cursor.getPosition());
					additionalPath.remove(0);
					tentativeSprintList.addAll(additionalPath);
					
				} else {
					tentativeSprintList = InGame.map.getPath(player.getPosition(), player.cursor.getPosition());
					tentativeSprintList.remove(0);
				}
				
			}
			
			if (!player.directionInput.equals(zero)) {
				player.cursor.Move(player.directionInput);
				player.inputPause();
	
				if (tentativeSprintList.contains(new Vector2f(player.cursor.getPosition()))) {
					splitSprintList();
				} else {
					tentativeSprintList.add(new Vector2f(player.cursor.getPosition()));
				}
			}
			
			if (tentativeSprintList.size() > 0 && KeyboardInputTools.confirm()) {
				commitSprintList();
				player.cameraLock = true;
				tentativeSprintList.clear();
				tilesSprinted = 0;
			}
	
			if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
				player.activateCommandMode("default");
				player.cursor.active = false;
				player.cameraLock = true;
				tentativeSprintList.clear();
			}
		}
	}

	@Override
	public void activate() {
		player.cursor.setPosition(player.getPosition());
		player.cursor.active = true;
		player.cursor.setColor(new Vector3f(0, 0, 1f));
		player.cameraLock = false;
		tentativeSprintList.clear();
	}

	private void splitSprintList() {
		int splitIndex = tentativeSprintList.indexOf(player.cursor.getPosition()) + 1;
		for (int i = splitIndex; i < tentativeSprintList.size(); i++) {
			sprintTiles.get(i).active = false;
		}
		tentativeSprintList = new ArrayList<Vector2f>(tentativeSprintList.subList(0, splitIndex));
	}

	private void commitSprintList() {
		sprinting = true;
		committedSprintList.clear();
		for (Vector2f vector2f : tentativeSprintList) {
			committedSprintList.add(vector2f);
		}
		tentativeSprintList.clear();
	}
}