package com.saltbox.latticesuit.databases;

import java.util.HashMap;

import org.joml.Vector2i;

public class TileDefinition {
	public TileDefinition(String type, HashMap<String, HashMap<String, String>> firstEntry) {
		HashMap<String, String> properties = firstEntry.get(type);
		this.type = Integer.parseInt(properties.get("type"));
		this.name = properties.get("name");
		this.description = properties.get("description");
		this.health = Integer.parseInt(properties.get("health"));
		this.blockingStatus = Integer.parseInt(properties.get("blockingstatus"));
		this.coverStatus = Integer.parseInt(properties.get("coverstatus"));
		this.visionStatus = Integer.parseInt(properties.get("visionstatus"));
		String[] spriteStrings = properties.get("spritecoords").split("\\s");
		this.sprites = new Vector2i[spriteStrings.length];
		for (int i = 0; i < spriteStrings.length; i++) {
			String[] splitCoordPair = spriteStrings[i].split(",");
			this.sprites[i] = new Vector2i(Integer.parseInt(splitCoordPair[0]), Integer.parseInt(splitCoordPair[1]));
		}
	}
	int type, health, coverStatus, blockingStatus, visionStatus;
	Vector2i[] sprites;
	String name, description;
	
	public int getType() {
		return type;
	}
	
	public int getHealth() {
		return health;
	}
	
	public int getCoverStatus() {
		return coverStatus;
	}
	
	public int getBlockingStatus() {
		return blockingStatus;
	}
	
	public int getVisionStatus() {
		return visionStatus;
	}
	
	public Vector2i[] getSprites() {
		return sprites;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
}
