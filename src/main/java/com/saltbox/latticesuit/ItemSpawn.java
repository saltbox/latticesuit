package com.saltbox.latticesuit;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.gamestates.InGame;

public class ItemSpawn extends EntitySpawn {

	public ItemSpawn(Vector2i position, String name) {
		super(position, name);
		spawnType = "item";
	}

	@Override
	public void spawn() {
		InGame.addEntityToList(DatabaseMediator.items.CreateItem(name, new Vector2f(position.x, position.y)));
		
	}
}
