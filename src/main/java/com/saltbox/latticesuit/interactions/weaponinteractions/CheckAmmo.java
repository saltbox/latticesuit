package com.saltbox.latticesuit.interactions.weaponinteractions;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.interactions.Interaction;
import com.saltbox.latticesuit.items.Weapon;

public class CheckAmmo extends Interaction {
	Weapon weapon;
	public CheckAmmo(Weapon w) {
		super("checkammo");
		this.weapon = w;
	}

	@Override
	public void doInteraction() {
		GameLog.writeLine(weapon.getAmmoStatus());
	}

	@Override
	public int getFrames() {
		// TODO Auto-generated method stub
		return 0;
	}

}
