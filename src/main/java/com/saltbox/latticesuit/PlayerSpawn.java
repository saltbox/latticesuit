package com.saltbox.latticesuit;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.gamestates.InGame;

public class PlayerSpawn extends ActorSpawn {

	public PlayerSpawn(Vector2i position) {
		super(position, "player");
	}

	@Override
	public void spawn() {
		InGame.player.setPosition(new Vector2f(position.x, position.y));
	}

}
