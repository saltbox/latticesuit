package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ENTER;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_ENTER;

import java.util.ArrayList;

import org.joml.Vector2f;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class SelectTarget extends PlayerCommand {
	Menu menu;
	ArrayList<Entity> targetableEntities = new ArrayList<Entity>();
	public boolean confirmSelection = false;

	public SelectTarget(Player player) {
		super(player);
		name = "selecttarget";
		setupMenu();
	}

	private void setupMenu() {
		menu = new Menu(new Vector2f(0, 0));
	}

	@Override
	public void draw(SpriteBatch sb) {
		menu.Draw(sb);
	}

	@Override
	public boolean doCommand() {
		if (confirmSelection) {
			Attack attackCommand = ((Attack) player.commands.get("attack"));
			attackCommand.target = targetableEntities.get(menu.getSelectedOption());
			attackCommand.confirmAttack = true;
			player.activateCommandMode("attack");
			confirmSelection = false;
			return true;
		}
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		menu.Update(gameTime);
		if (KeyboardInputTools.confirm()) {
			confirmSelection = true;
		}

		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
			player.cursor.active = false;
		}
	}

	@Override
	public void activate() {
		targetableEntities.clear();
		player.cursor.active = true;
		menu.options.clear();
		getTargetableEntitiesAtTile(player.cursor.getPosition());
		populateMenuOptions();
	}

	private void populateMenuOptions() {
		for (Entity e : targetableEntities) {
			if (e.getName() != null && e.getName() != "") {
				menu.options.add(e.getName());
			} else {
				menu.options.add("NO NAME");
			}
		}
	}

	private void getTargetableEntitiesAtTile(Vector2f cursorPosition) {
		for (Entity e : InGame.targetableEntities) {
			if (e.getPosition().equals(cursorPosition)) {
				targetableEntities.add(e);
			}
		}
		targetableEntities.add(InGame.map);
	}

}
