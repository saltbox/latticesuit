package com.saltbox.latticesuit.UI;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;

public class CursorPositionReadout extends UIGameObject {
	
	public CursorPositionReadout(Vector2f position, float zDepth) {
		super(position, zDepth, new Vector2i(11, 1));
	}

	Vector2f mousePosition = new Vector2f();
	@Override
	public void Update(GameTime gameTime) {
		InGame.map.getMousePosInMap(mousePosition);
		setSpriteLine(new Vector2i(), mousePosition.x + " " + mousePosition.y + "         ", new Vector3f(1, 1, 1));
		
	}

}
