package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.UI.IMenuOptionSelect;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class SelectItemFromInventory extends InventoryManagementCommand {
	Menu inventoryMenu;
	Menu equipmentMenu;
	boolean firstFrameActive = true;
	
	public String playerCommandToReturnTo = "default";
	Vector2f equipmentMenuOffset = new Vector2f(24, 0);
	
	public SelectItemFromInventory(Player player) {
		super(player);
		name = "selectitemfrominventory";
		addMenu(player.inventory);
		addMenu(player.equipment);
		inventoryMenu = menus.get(0);
		equipmentMenu = menus.get(1);
	}
	
	@Override
	public void activate() {
		super.activate();
		firstFrameActive = true;
		inventoryMenu.selected = true;
		if (inventoryMenu.options.size() == 0) {
			inventoryMenu.options.add("Nothing in inventory");
		}
		
		if (player.getPrimaryWeapon() == null) {
			equipmentMenu.options.add(0, "No primary weapon");
		}
		
		if (player.getSecondaryWeapon() == null) {
			equipmentMenu.options.add(1, "No secondary weapon");
		}
	}

	@Override
	public void confirmSelection() {
		super.confirmSelection();
		player.activateCommandMode(playerCommandToReturnTo);
		autoUpdateMenuContents();
	}
	
	@Override 
	public void update(GameTime gameTime) {
		super.update(gameTime);
		if (!firstFrameActive && KeyboardInput.isKeyDown(GLFW_KEY_I) && !KeyboardInput.isKeyDown(GLFW_KEY_I, true)) {
			if (player.inventoryContainsItemInSlot(selectedInventory, selectedRow)) {
				((Interact)player.commands.get("interact")).entity = player.getItemAtSlot(selectedInventory, selectedRow);
				player.activateCommandMode("interact");
			}
		}
		firstFrameActive = false;
	}
}
