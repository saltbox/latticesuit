package com.saltbox.latticesuit.playercommands;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.UI.TileColorer;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;
import java.util.Vector;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class Flash extends PlayerCommand {
	float radius = 10f;
	public Flash(Player player) {
		super(player);
	}

	@Override
	public void draw(SpriteBatch sb) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}
	
	Vector2f mousePosition = new Vector2f();
	Vector2f previousCursorPosition = new Vector2f();
	Vector2f zero = new Vector2f();
	@Override
	public void update(GameTime gameTime) {
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
			player.cursor.active = false;
			clearFlashableTiles();
		}
		
		if (MouseInput.isButtonDown(0) && !MouseInput.isButtonDown(0, true)) {
			InGame.map.getMousePosInMap(mousePosition);
			if (mousePosition.distance(player.getPosition()) < radius) {
				updateCursorPosition(mousePosition);
				if (player.cursor.getPosition().equals(previousCursorPosition)) {
					doFlash();
				}
			}
		}
		
		if (!player.directionInput.equals(zero)) {
			Vector2f newCell = new Vector2f(player.cursor.getPosition()).add(player.directionInput);
			if (newCell.distance(player.getPosition()) < radius) {
				updateCursorPosition(newCell);
				player.inputPause();
			}
		}
		
		if (KeyboardInputTools.confirm()) {
			doFlash();
		}

	}

	private void updateCursorPosition(Vector2f newPosition) {
		previousCursorPosition = new Vector2f(player.cursor.getPosition());
		player.cursor.setPosition(newPosition);
	}

	private void doFlash() {
		player.setPosition(player.cursor.getPosition());
		player.activateCommandMode("default");
		player.cursor.active = false;
		clearFlashableTiles();
	}

	@Override
	public void activate() {
		player.cursor.active = true;
		player.cursor.setPosition(player.getPosition());
		colorFlashableTiles();
	}

	private void colorFlashableTiles() {
		ArrayList<Vector2f> flashableTiles = new ArrayList<Vector2f>();
		for (float x = player.getPosition().x - radius; x <= player.getPosition().x + radius; x++) {
			for (float y = player.getPosition().y - radius; y <= player.getPosition().y + radius; y++) {
				Vector2f t = new Vector2f(x, y);
				if (InGame.map.tileIsInBounds(t.x, t.y) && player.getPosition().distance(t) < radius) {
					flashableTiles.add(t);
				}
			}
		}
		TileColorer.SetTiles(flashableTiles);
		TileColorer.SetColor(new Vector3f(0.35f, 1f, 0.35f));
		TileColorer.apply();
	}
	
	@SuppressWarnings("static-method")
	private void clearFlashableTiles() {
		TileColorer.clear();
	}

}
