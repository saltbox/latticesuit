package com.saltbox.latticesuit.gamestates;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;

import com.saltbox.latticesuit.ConsoleCommands;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.CursorPositionReadout;
import com.saltbox.latticesuit.UI.PlayerUI;
import com.saltbox.latticesuit.UI.ShotDisplayer;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.SpriteBatch;
import com.saltboxgames.latticelight.utils.FPSGameObject;

public class InGame extends GameState {
	public static Player player;
	public static PlayerUI playerUI;
	
	public static ShotDisplayer shotDisplayer;
	public static Map map;
	public static List<Entity> entities;
	public static List<Entity> targetableEntities;
	
	FPSGameObject fps;
	CursorPositionReadout cursorPositionReadout;
	int updateCounter = 0;
	
	@Override
	public void Update(GameTime gameTime) {
		if (player.actionFrames > 0) {
			for (int i = 0; i < entities.size(); i++) {
				entities.get(i).Update(gameTime);
			}
		} else {
			player.Update(gameTime);
		}
		
		if (player.actionFrames > 0) {
			shotDisplayer.Update();
		}
		playerUI.Update(gameTime);
		fps.Update(gameTime);
		map.Update(gameTime);
		cursorPositionReadout.Update(gameTime);
	}
	
	Vector2f playerPosition = new Vector2f();

	@Override
	public void Draw(SpriteBatch sb) {
		player.getPosition(playerPosition);
		fps.Draw(sb);
		player.Draw(sb);
		for (GameObject gameObject : entities) {
			gameObject.Draw(sb);
		}
		shotDisplayer.drawAllShots();
		map.Draw(sb);
		playerUI.Draw(sb);
		cursorPositionReadout.Draw(sb);

	}

	@Override
	public void Start() {
		entities = new ArrayList<Entity>();
		targetableEntities = new ArrayList<Entity>();
		player = new Player();
		playerUI = new PlayerUI(player);
		ConsoleCommands.defineConsoleCommands(LatticeSuit.instance, player);
		addEntityToList(player);
	
		map = new Map();
		shotDisplayer = new ShotDisplayer();
		
		fps = new FPSGameObject(new Vector2f(LatticeSuit.instance.getSize().x - 10, 0), 100, LatticeSuit.instance);
		cursorPositionReadout = new CursorPositionReadout(new Vector2f(), 10f);
	}
	
	public static void addEntityToList(Entity e) {
		entities.add(e);
		if (e.targetable) {
			targetableEntities.add(e);
		}
	}
	
	public static void removeEntityFromList(Entity e) {
		entities.remove(e);
		targetableEntities.remove(e);
	}
	
	public static ArrayList<Entity> getAllPickupableEntitiesAtPosition(Vector2f position) {
		ArrayList<Entity> list = new ArrayList<Entity>();
		for (Entity entity : entities) {
			if(position.equals(entity.getPosition()) && entity.pickupable) {
				list.add(entity);
			}
		}
		return list;
	}

}
