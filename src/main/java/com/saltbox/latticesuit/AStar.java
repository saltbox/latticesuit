package com.saltbox.latticesuit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.battlegroup.Fireteam;
import com.saltbox.latticesuit.battlegroup.IFireteamMember;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.gamestates.InGame;

public class AStar {
	
	static DefaultNeighborGetter defaultNeighborGetter = new DefaultNeighborGetter();
	
	static NeighborGetterWithoutSquadmates neighborsExcludingSquadmates = new NeighborGetterWithoutSquadmates();
	
	public static ArrayList<Vector2f> FindPath(Vector2f origin, Vector2f destination) {
		return FindPath(origin, destination, defaultNeighborGetter);
	}
	
	public static ArrayList<Vector2f> FindPathAroundSquadmates(Vector2f origin, Vector2f destination, Fireteam fireteam, IFireteamMember sender) {
		neighborsExcludingSquadmates.fireteam = fireteam;
		neighborsExcludingSquadmates.sender = sender;
		return FindPath(origin, destination, neighborsExcludingSquadmates);
	}
	
	public static ArrayList<Vector2f> FindPath(Vector2f origin, Vector2f destination, NeighborGetter neighborGetter) {
		origin = new Vector2f(origin);
		destination = new Vector2f(destination);
		LinkedList<FrontierCell> frontier = new LinkedList<FrontierCell>();
		HashMap<Vector2f, Vector2f> cameFrom = new HashMap<Vector2f, Vector2f>();
		HashMap<Vector2f, Float> costSoFar = new HashMap<Vector2f, Float>();
		costSoFar.put(origin, 0f);
		
		frontier.add(new FrontierCell(origin, 0));
		
		while(!frontier.isEmpty()) {
			Vector2f current = frontier.removeFirst().pos;
			if (current.equals(destination)) {
				return finalizePath(cameFrom, origin, destination);
			}
			for (Vector2f next : neighborGetter.get(current)) {
				float newCost = costSoFar.get(current).floatValue() + InGame.map.getAStarPathCost(current, next);
				if (!costSoFar.containsKey(next) || newCost < costSoFar.get(next)) {
					costSoFar.put(next, newCost);
					float priority = newCost + heuristic(destination, next);
					frontier.add(new FrontierCell(next, priority));
					cameFrom.put(next, current);
				}
			}
		}
		
		return finalizePath(cameFrom, origin, destination);
	}
	
	
	
	static ArrayList<Vector2f> finalizePath(HashMap<Vector2f, Vector2f> cameFrom, Vector2f origin, Vector2f destination) {
		ArrayList<Vector2f> path = new ArrayList<Vector2f>();
		path.add(destination);
		while(true) {
			if(path.get(0) == null) { //i think this is the no-path-found case?
				path.clear();
				path.add(origin);
				return path;
			}
			
			if (path.get(0).equals(origin)) {
				return path;
			} 
			path.add(0, cameFrom.get(path.get(0)));
		}
	}
	
	static ArrayList<Vector2f> getNeighbors(Vector2f cell) {
		return getNeighbors(cell, false);
	}
	
	static ArrayList<Vector2f> getNeighbors(Vector2f cell, boolean tunneling) {
		return InGame.map.getPathableNeighbors(cell, tunneling);
	}
	
	static float heuristic(Vector2f a, Vector2f b) {
		return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
	}
}

class FrontierCell {
	public Vector2f pos;
	public float priority;
	public FrontierCell(Vector2f pos, float priority) {
		this.pos = pos;
		this.priority = priority;
	}
}

interface NeighborGetter {
	public ArrayList<Vector2f> get(Vector2f cell);
}

class DefaultNeighborGetter implements NeighborGetter {
	@Override
	public ArrayList<Vector2f> get(Vector2f cell) {
		return InGame.map.getPathableNeighbors(cell);
	}
}

class NeighborGetterWithoutSquadmates implements NeighborGetter {
	public Fireteam fireteam;
	public IFireteamMember sender;
	@Override
	public ArrayList<Vector2f> get(Vector2f cell) {
		ArrayList<Vector2f> neighbors = InGame.map.getPathableNeighbors(cell);
		for(IFireteamMember ftm : fireteam.members) {
			for (int i = 0; i < neighbors.size(); i++) {
				if (ftm.getPosition().equals(neighbors.get(i))) {
					neighbors.remove(i);
					i--;
				}
			}
		}
		return neighbors;
	}
}
