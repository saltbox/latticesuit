package com.saltbox.latticesuit.battlegroup;

import org.joml.Vector2f;

import com.saltbox.latticesuit.AIStates.AIState;

public interface IFireteamMember {
	public Vector2f getPosition();
	public boolean isAlive();
	public void setFireteam(Fireteam fireteam);
	public Fireteam getFireteam();
	public AIState getActiveState();
	public void setState(String newState);
	public boolean canSeeTarget();
	public boolean noAlliesInFrontOfTarget();
	public Vector2f getTargetPosition();
	public void setTargetPosition(Vector2f newPosition, boolean notifyFireteam);
}
