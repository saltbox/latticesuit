package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class DefaultPlayerCommand extends PlayerCommand {

	public DefaultPlayerCommand(Player player) {
		super(player);
		name = "default";
	}

	@Override
	public void draw(SpriteBatch sb) {

	}

	@Override
	public boolean doCommand() {
		if (player.commandMode.equals("default")) {
			if (player.Move(player.directionInput)) {
				player.TakeTurn(player.moveFrames);
				player.inputPause();
				return true;
			}
			
			if (MouseInput.isButtonDown(1)) {
				Vector2f v = new Vector2f();
				player.getMouseInputDirection(v);
				if (player.Move(v)) {
					player.TakeTurn(player.moveFrames);
					player.inputPause();
				}
			}
		}
		return false;
	}
	
	

	Vector2i targetMouseMoveCell = new Vector2i();
	@Override
	public void update(GameTime gameTime) {
		if (KeyboardInput.isKeyDown(GLFW_KEY_S) && !KeyboardInput.isKeyDown(GLFW_KEY_S, true)) {
			player.activateCommandMode("sprint");
		}

		if (KeyboardInput.isKeyDown(GLFW_KEY_A) && !KeyboardInput.isKeyDown(GLFW_KEY_A, true)) {
			if (player.commandMode != "selecttarget") {
				player.activateCommandMode("attack");
			}
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_W) && !KeyboardInput.isKeyDown(GLFW_KEY_W, true)) {
			player.TakeTurn(player.moveFrames);
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_I) && !KeyboardInput.isKeyDown(GLFW_KEY_I, true)) {
			player.activateCommandMode("selectitemfrominventory");
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_G) && !KeyboardInput.isKeyDown(GLFW_KEY_G, true)) {
			player.activateCommandMode("get");
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_E) && !KeyboardInput.isKeyDown(GLFW_KEY_E, true)) {
			player.activateCommandMode("equip");
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_L) && !KeyboardInput.isKeyDown(GLFW_KEY_L, true)) {
			player.activateCommandMode("lookaround");
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_F) && !KeyboardInput.isKeyDown(GLFW_KEY_F, true)) {
			player.activateCommandMode("flash");
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_H) && !KeyboardInput.isKeyDown(GLFW_KEY_H, true)) {
			player.activateCommandMode("history");
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_D) && !KeyboardInput.isKeyDown(GLFW_KEY_D, true)) {
			player.activateCommandMode("drop");
		}
		
		for (int i = 0; i < player.equipment.size() && i <= GLFW_KEY_9 ; i++) {
			if (KeyboardInput.isKeyDown(i + GLFW_KEY_1) && !KeyboardInput.isKeyDown(i + GLFW_KEY_1, true)) {
				if (player.equipment.get(i) != null) {
					((Interact)player.commands.get("interact")).entity = player.equipment.get(i);
					player.activateCommandMode("interact");
				}
			}
		}
	}

	@Override
	public void activate() {
		player.cameraLock = true;

	}

}
