package com.saltbox.latticesuit.interactions;

public abstract class Interaction {
	public Interaction(String name) {
		this.name = name;
	}
	public String name;
	public abstract void doInteraction();
	public abstract int getFrames();
}
