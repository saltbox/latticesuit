package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.UI.IMenuOptionSelect;
import com.saltbox.latticesuit.UI.PlayerUI;
import com.saltbox.latticesuit.UI.Textbox;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.interactions.Interaction;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Interact extends PlayerCommand {
	public Entity entity;
	Menu interactionMenu;
	
	public Interact(Player player) {
		super(player);
		name = "interact";
		interactionMenu = new Menu();
		interactionMenu.menuOptionSelect = new IMenuOptionSelect() {
			@Override
			public void call(int option) {
				entity.interactions.get(option).doInteraction();;
				
			}
		};
		Vector2i interactionMenuSize = new Vector2i();
		interactionMenu.getSize(interactionMenuSize);
		interactionMenu.setPosition(new Vector2f(0, LatticeSuit.GetSize().y - (PlayerUI.weaponLines + interactionMenuSize.y)));
	}

	@Override
	public void draw(SpriteBatch sb) {
		interactionMenu.Draw(sb);
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		interactionMenu.Update(gameTime);
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
		}
	}

	@Override
	public void activate() {
		interactionMenu.options.clear();
		for (Interaction i : entity.interactions) {
			interactionMenu.options.add(i.name);
		}
	}

}
