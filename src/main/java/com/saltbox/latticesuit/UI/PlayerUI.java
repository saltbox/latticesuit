package com.saltbox.latticesuit.UI;



import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Component;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class PlayerUI{
	Player player;
	Textbox weaponStatus;
	Textbox weaponStatusLabels;
	Textbox eventLog;
	Textbox eventLogLarge;
	Textbox componentStatus;
	Textbox playerStatus;
	Textbox framesSinceStart;
	
	public static int weaponLabelWidth = 10;
	public static int weaponTextWidth = 10;
	public static int weaponLines = 4;
	public static int componentLines = 6;
	public static int componentWidth = 20;
	public static int spaceAfterHistory = 30;
	public static int playerStatusLines = 6;
	public static boolean historyLarge = false;
	
	Vector2i gameSize;
	public PlayerUI (Player player) {
		this.player = player;
		gameSize = LatticeSuit.GetSize();
		weaponStatus = new Textbox(new Vector2f(weaponLabelWidth, gameSize.y - weaponLines), 7f, new Vector2i(weaponTextWidth, weaponLines));
		weaponStatusLabels = new Textbox(new Vector2f(0, gameSize.y - weaponLines), 7f, new Vector2i(weaponLabelWidth, weaponLines));
		componentStatus = new Textbox(new Vector2f(gameSize.x - spaceAfterHistory, gameSize.y - componentLines), 7f, new Vector2i(componentWidth, componentLines));
		playerStatus = new Textbox(new Vector2f(gameSize.x - spaceAfterHistory + componentWidth, gameSize.y - playerStatusLines), 7f, new Vector2i(gameSize.x - spaceAfterHistory, playerStatusLines));
		framesSinceStart = new Textbox(new Vector2f(gameSize.x - 6, 1), 7f, new Vector2i(6, 1));
		
		weaponStatusLabels.setSpriteText(new Vector2i(0, 0), "primary");
		weaponStatusLabels.setSpriteText(new Vector2i(0, 1), "ammo");
		weaponStatusLabels.setSpriteText(new Vector2i(0, 2), "secondary");
		weaponStatusLabels.setSpriteText(new Vector2i(0, 3), "ammo");
		
		eventLog = new Textbox(new Vector2f(weaponTextWidth + weaponLabelWidth, gameSize.y - weaponLines), 7f, new Vector2i(Math.round(gameSize.x) - (weaponLabelWidth + weaponTextWidth) - spaceAfterHistory, weaponLines));
		eventLogLarge = new Textbox(new Vector2f(weaponTextWidth + weaponLabelWidth, 0), 7f, new Vector2i(Math.round(gameSize.x) - (weaponLabelWidth + weaponTextWidth) - spaceAfterHistory, gameSize.y));
		GameLog.playerLog = eventLog;
		GameLog.playerLogLarge = eventLogLarge;
	}

	public void Update(GameTime gameTime) {
		weaponStatus.clear();
		updateWeaponStatus(player.getPrimaryWeapon(), new Vector2i(0, 0));
		updateWeaponStatus(player.getSecondaryWeapon(), new Vector2i(0, 2));
		if (player.hardsuit != null) {
			for(int i = 0; i < player.hardsuit.components.size(); i++) {
				Component c = player.hardsuit.components.get(i); 
				componentStatus.setSpriteLine(new Vector2i(0, i), c.name + ": " + (c.health < 0 ? 0 : c.health) + "         ", new Vector3f(1, 1, 1));
			}
		}
		playerStatus.setSpriteLine(new Vector2i(0, 0), "body: " + player.health + "     ", new Vector3f(1, 1, 1));
		framesSinceStart.setSpriteText(new Vector2i(0, 0), "" + player.framesSinceStart);
	}

	private void updateWeaponStatus(Entity weapon, Vector2i origin) {
		if (weapon != null) {
			weaponStatus.setSpriteText(origin, weapon.getName());
			if (weapon instanceof Weapon) {
				weaponStatus.setSpriteText(new Vector2i(origin.x, origin.y + 1), ((Weapon)weapon).getShortAmmoStatus());
			}
		} else {
			weaponStatus.setSpriteText(origin, "-");
		}
	}
	
	public void Draw(SpriteBatch sb) {
		weaponStatusLabels.Draw(sb);
		weaponStatus.Draw(sb);
		componentStatus.Draw(sb);
		playerStatus.Draw(sb);
		framesSinceStart.Draw(sb);
		if(historyLarge) {
			eventLogLarge.Draw(sb);
		} else {
			eventLog.Draw(sb);
		}
	}
}
