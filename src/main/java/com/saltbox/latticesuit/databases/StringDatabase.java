package com.saltbox.latticesuit.databases;

import java.util.HashMap;

public class StringDatabase extends Database {
	HashMap<String, HashMap<String, String>> firearmStatusDescriptions;
	public StringDatabase() {
		super("Data/Definitions/Items/Descriptions");
		firearmStatusDescriptions = entries.get("firearmstatusdescriptions");
	}

	public String getWeaponAppearanceString(float health, float maxHealth) {
		String healthRatioRounded = String.valueOf(clamp(0, maxHealth, (float)Math.ceil((health / maxHealth) * 10f) / 10f));
		return firearmStatusDescriptions.get(healthRatioRounded).get("appearance");
	}
	
	public String getWeaponInternalConditionString(float health, float maxHealth) {
		String healthRatioRounded = String.valueOf(clamp(0, maxHealth, (float)Math.ceil((health / maxHealth) * 10f) / 10f));
		return firearmStatusDescriptions.get(healthRatioRounded).get("test");
	}
	
	public String getWeaponAmmoStatusString(int ammo, int maxAmmo) {
		return "It's at " + ammo + "/" + maxAmmo + " rounds.";
	}
	
	private float clamp(float min, float max, float var) {
		return Math.max(min,  Math.min(max, var));
	}
}
