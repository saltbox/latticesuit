package com.saltbox.latticesuit;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;

import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.gamestates.InGame;

public class Raycast {
	
	public static enum Mode {
		COVER, MOVEMENT, VISION, PLAYER, ENEMY;
		public final int maskIndex;
		Mode() {
			this.maskIndex = 1 << this.ordinal();
		}
	}
	
	public static Map map;
	
	static int maximumRaycastDistance = 100000;
	static int maximumCasts = 50;
	static int maximumPathDistance = 500;
	static Random deflectionRandom = new Random();
	static boolean includeCorners = true;
	
	
	
	public static RaycastHit raycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity) {
		return raycast(origin, target, travelledCells, originEntity, Mode.VISION);
	}
	
	public static RaycastHit raycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity, int modeMask) {
		return raycast(origin, target, travelledCells, originEntity, modeMask, maximumRaycastDistance, 0, 0, true);
	}

	public static RaycastHit raycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity, Mode mode) {
		return raycast(origin, target, travelledCells, originEntity, mode.maskIndex, maximumRaycastDistance, 0, 0, true);
	}
	
	public static RaycastHit meleeRaycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity, int range, int damage) {
		return raycast(origin, target, travelledCells, originEntity, Mode.COVER.maskIndex | Mode.PLAYER.maskIndex | Mode.ENEMY.maskIndex, range, damage, 0, false);
	}
	
	public static RaycastHit bulletRaycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity, int damage) {
		return bulletRaycast(origin, target, travelledCells, originEntity, damage, 0); 
	}
	
	public static RaycastHit bulletRaycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity, int damage, int castCount) {
		RaycastHit hit = raycast(origin, target, travelledCells, originEntity, Mode.COVER.maskIndex | Mode.PLAYER.maskIndex | Mode.ENEMY.maskIndex, maximumRaycastDistance, damage, castCount + 1, true);
		if (travelledCells != null && travelledCells.size() > 0) {
			travelledCells.remove(travelledCells.size() - 1);
		}
		return hit; 
	}
	
	// some of this stuff is from
	// http://www.saltgames.com/articles/lineOfSight/lineOfSightDemo.js
	public static RaycastHit raycast(Vector2f origin, Vector2f target, ArrayList<Vector2f> travelledCells, Entity originEntity, int modeMask, int maxRange, int damage, int castCount, boolean includeCorners) {
		if (origin.equals(target)) {
			return null;
		}
		RaycastHit out = null;
		for (Vector2f originVariant : getCellOriginVariants(origin, target)) {
			Vector2f offsetOrigin = new Vector2f(origin).add(originVariant);
			Vector2f offsetTarget = new Vector2f(target).add(originVariant);
			RaycastState rstate = new RaycastState(offsetOrigin, offsetTarget, modeMask, maxRange, damage, castCount);
			RaycastHit hit = new RaycastHit();
			hit.remainingDamage = damage;
			Vector2f cell = new Vector2f(offsetOrigin);
			Vector2f intermediate = new Vector2f(offsetOrigin);
			Vector2f previousCell = new Vector2f(offsetOrigin);
			
			while (rstate.castSteps < rstate.maxRange) {
				preCheckForHit(travelledCells, cell, previousCell, intermediate);
				if (checkForHit(rstate, hit, cell, previousCell, originEntity)) {
					RaycastHit newOut = respondToHit(rstate, hit, travelledCells);
					if (out == null || newOut.hitPosition.distance(offsetTarget) < out.hitPosition.distance(offsetTarget)) {
						out = newOut;
						break;
					}
				}
				updatePreviousCell(cell, previousCell);
				updateIntermediate(rstate, intermediate);
				float twoError = stepX(rstate);
				if (includeCorners) {
					preCheckForHit(travelledCells, cell, previousCell, intermediate);
					if (checkForHit(rstate, hit, cell, previousCell, originEntity)) {
						RaycastHit newOut = respondToHit(rstate, hit, travelledCells);
						if (out == null || newOut.hitPosition.distance(offsetTarget) < out.hitPosition.distance(offsetTarget)) {
							out = newOut;
							break;
						}
					}
					updatePreviousCell(cell, previousCell);
					updateIntermediate(rstate, intermediate);
				}
				stepY(rstate, twoError);
			}
		}
		return out;
	}
	
	private static Vector2f[] getCellOriginVariants(Vector2f origin, Vector2f target) {
		Vector2f[] variants = new Vector2f[1];
		variants[0] = new Vector2f(0, 0);
		//variants[1] = new Vector2f(0f, 0.5001f);
		//variants[2] = new Vector2f(0f, -0.5001f);
		return variants;
	}

	private static RaycastHit respondToHit (RaycastState rstate, RaycastHit hit, ArrayList<Vector2f> travelledCells) {
		applyDamageToHitEntity(hit);
		trimLastTravelledCell(rstate, travelledCells);
		if (!raycastPenetrates(rstate) || hit.remainingDamage <= 0) {
			return hit;
		}
		return hit;
	}

	private static void preCheckForHit(ArrayList<Vector2f> travelledCells, Vector2f cell, Vector2f previousCell, Vector2f intermediate) {
		updateCell(cell, intermediate);
		addCellToTravelledCells(travelledCells, cell, previousCell);
	}

	private static void updateCell(Vector2f cell, Vector2f intermediate) {
		cell.x = Math.round(intermediate.x);
		cell.y = Math.round(intermediate.y);
	}

	private static boolean shotShouldDeflect(RaycastState rstate, RaycastHit hit) {
		return hit.hitEntity != null && rstate.castCount < maximumCasts && rollForDeflect(hit);
	}

	private static void trimLastTravelledCell(RaycastState rstate, ArrayList<Vector2f> travelledCells) {
		if (travelledCells != null && !raycastTravelsToHitCell(rstate)) {
			travelledCells.remove(travelledCells.size() - 1);
		}
	}

	private static void updateIntermediate(RaycastState rstate, Vector2f intermediate) {
		rstate.castSteps++;
		intermediate.set(rstate.xGrid, rstate.yGrid);		
	}

	private static void stepY(RaycastState rstate, float twoError) {
		if (twoError < rstate.deltaX) {
			rstate.error += rstate.deltaX;
			rstate.yGrid += rstate.yStep;
		}
	}

	private static float stepX(RaycastState rstate) {
		float twoError = 2 * rstate.error;
		if (twoError > (-1 * rstate.deltaY)) {
			rstate.error -= rstate.deltaY;
			rstate.xGrid += rstate.xStep;
		}
		return twoError;
	}

	private static boolean raycastHitsValidMaptile(RaycastHit hit) {
		return hit.hitEntity == map && map.tileIsInBounds(hit.hitPosition.x, hit.hitPosition.y);
	}

	private static boolean rollForDeflect(RaycastHit hit) {
		return deflectionRandom.nextFloat() < hit.hitEntity.getDeflectionChance();
	}

	private static boolean raycastIsBullet(int modeMask) {
		return maskContainsLayer(modeMask, Mode.COVER);
	}

	private static void applyDamageToHitEntity(RaycastHit hit) {
		if (hit.hitEntity != null) {
			int damageToDeal = hit.remainingDamage;
			if (raycastHitsValidMaptile(hit)) {
				hit.remainingDamage -= map.damageMap[(int) (hit.hitPosition.x + (hit.hitPosition.y * map.getSize().x))];
			} else {
				hit.remainingDamage -= hit.hitEntity.health;
			}
		
			if (hit.hitEntity == map) {
				hit.hitEntity.Damage(Math.max(damageToDeal, 0), hit.hitPosition);
			} else {
				hit.hitEntity.Damage(Math.max(damageToDeal, 0));
			}
		}
	}

	private static boolean raycastPenetrates(RaycastState rstate) {
		return maskContainsLayer(rstate.modeMask, Mode.COVER);
	}
	
	private static boolean raycastTravelsToHitCell(RaycastState rstate) {
		return maskContainsLayer(rstate.modeMask, Mode.VISION) || raycastPenetrates(rstate);
	}

	private static RaycastHit ricochet(RaycastState rstate, RaycastHit hit, ArrayList<Vector2f> travelledCells) {
		if (travelledCells != null && travelledCells.size() > 0) {
			travelledCells.remove(travelledCells.size() - 1);
		}
		hit.hitEntity = null;
		return bulletRaycast(hit.hitPosition, getRicochetVector(hit), travelledCells, hit.hitEntity, hit.remainingDamage, rstate.castCount + 1);
	}
	
	private static Vector2f getRicochetVector(RaycastHit hit) {
		return new Vector2f(hit.hitPosition).add(new Vector2f(deflectionRandom.nextInt(20) - 10, deflectionRandom.nextInt(20) - 10));
	}
	
	private static boolean maskContainsLayer(int mode, int layer) {
		return (mode & layer) != 0;
	}
	
	private static boolean maskContainsLayer(int mode, Mode raycastMode) {
		return maskContainsLayer(mode, raycastMode.maskIndex);
	}

	private static boolean checkForHit(RaycastState rstate, RaycastHit hit, Vector2f cell, Vector2f previousCell, Entity originEntity) {
		int index = (int) (cell.x + cell.y * map.getSize().x);
		if (cell.x < 0 || cell.y < 0 || cell.x > map.getSize().x - 1 || cell.y > map.getSize().y - 1) {
			hit.hitPosition = previousCell;
			return true;
		}
		for (Mode rm : Mode.values()) {
			if (maskContainsLayer(rstate.modeMask, rm)) {
				int[] m = getMapByRaycastMode(rm); 
				if (m != null && m[index] != 0) {
					hit.hitEntity = map;
					hit.hitPosition = cell;
					return true;
				}
			}
			
			for (Entity e : InGame.entities) {
				if (e.raycastable && (e == null || !e.equals(originEntity)) && maskContainsLayer(rstate.modeMask, rm) && maskContainsLayer(e.raycastMask, rm)) { 					
					if (e.getPosition().equals(cell)) {
						hit.hitEntity = e;
						hit.hitPosition = cell;
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static int[] getMapByRaycastMode(Mode m) {
		switch(m.ordinal()) {
			case 0: return map.coverMap;
			case 1: return map.movementMap; 
			case 2: return map.visionMap;
			case 3: return map.visionMap;
			default: return null;
		}
	}

	private static void updatePreviousCell(Vector2f cell, Vector2f previousCell) {
		previousCell.set(cell);
	}

	private static void addCellToTravelledCells(ArrayList<Vector2f> travelledCells, Vector2f cell, Vector2f previousCell) {
		if (travelledCells != null && (previousCell == null || !previousCell.equals(cell))) {
			travelledCells.add(new Vector2f(cell));
		}
	}
}
