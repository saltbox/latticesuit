package com.saltbox.latticesuit.entities.actors;

import java.util.ArrayList;
import java.util.Random;

public class Hardsuit {
	public static Random suitRandom = new Random();
	public ArrayList<Component> components = new ArrayList<Component>();

	public Hardsuit() {
		components.add(new Component("head armor"));
		components.add(new Component("left arm armor"));
		components.add(new Component("right arm armor"));
		components.add(new Component("left leg armor"));
		components.add(new Component("right leg armor"));
		components.add(new Component("torso armor", 100, 0.5f));
	}
	
	public ComponentHit Damage(int damage) {
		float r = suitRandom.nextFloat() * getTotalComponentHitChance();
		float hitRegion = 0;
		for(Component component : components) {
			hitRegion += component.hitChance;
			if (hitRegion > r) {
				return component.Damage(damage);
			}
		}
		return null;
	}
	
	private float getTotalComponentHitChance() {
		float totalChance = 0;
		for(Component component : components) {
			totalChance += component.hitChance;
		}
		return totalChance;
	}
}