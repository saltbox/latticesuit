package com.saltbox.latticesuit.AIStates;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;

import com.saltbox.latticesuit.AStar;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.entities.actors.AIActor;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.logging.Logger;

public class FirefightReposition extends AIState {
	final int randomMoveRadius = 5; 
	final int repositionTriesPerTurn = 5; 
	static Random repositionRandom = new Random();
	
	ArrayList<Vector2f> pathToTarget;
	Vector2f rePosition = null;
	
	public FirefightReposition(AIActor actor) {
		super(actor);
	}

	@Override
	public void Start() {
		rePosition = null;
	}

	@Override
	public void Update(GameTime gt) {
		if (pathExists()) {
			followPathToBetterPosition();
		} else {
			tryToFindBetterPosition();
		}
	}

	private void followPathToBetterPosition() {
		if (!actor.getPosition().equals(rePosition) && pathToTarget != null && actor.targetPosition != null) {
			actor.Move(pathToTarget.remove(0).sub(actor.getPosition()));
			RaycastHit hit = Raycast.raycast(actor.getPosition(), actor.targetPosition, null, actor, Raycast.Mode.VISION.maskIndex | Raycast.Mode.PLAYER.maskIndex | Raycast.Mode.ENEMY.maskIndex);
			if (hit != null && hit.hitEntity != null && hit.hitEntity.equals(InGame.player)) {
				pathToTarget.clear();
				actor.setState("firefight");
			}
		} else {
			actor.setState("firefight");
		}
	}

	private void tryToFindBetterPosition() {
		for (int i = 0; i < repositionTriesPerTurn; i++) {
			rePosition = getRepositionCell();
			RaycastHit hit = Raycast.raycast(rePosition, actor.targetPosition, null, actor, Raycast.Mode.VISION.maskIndex | Raycast.Mode.PLAYER.maskIndex | Raycast.Mode.ENEMY.maskIndex);
			if (hit != null && hit.hitEntity != null && hit.hitEntity.equals(InGame.player)) {
				pathToTarget = AStar.FindPathAroundSquadmates(actor.getPosition(), rePosition, actor.getFireteam(), actor);
			} else {
				actor.setState("assault");
			}
		}
	}
	
	private Vector2f getRepositionCell() {
		Vector2f newCell = new Vector2f(actor.getPosition());
		newCell.x += repositionRandom.nextInt(randomMoveRadius * 2) - randomMoveRadius;
		newCell.y += repositionRandom.nextInt(randomMoveRadius * 2) - randomMoveRadius;
		if (newCell.equals(actor.getPosition())) {
			newCell.add(new Vector2f(1, 1));
		}
		return newCell;
	}
	
	private boolean pathExists() {
		return pathToTarget != null && !pathToTarget.isEmpty();
	}
}
