package com.saltbox.latticesuit.playercommands;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

import static org.lwjgl.glfw.GLFW.*;

public class History extends PlayerCommand {

	public History(Player player) {
		super(player);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(SpriteBatch sb) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			GameLog.retract();
			player.activateCommandMode("default");
		}
	}

	@Override
	public void activate() {
		GameLog.expand();
	}
}
