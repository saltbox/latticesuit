package com.saltbox.latticesuit.entities.actors;

import java.util.ArrayList;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;

public class Actor extends Entity {
	final int INVENTORY = 0, EQUIPMENT = 1;
	final int PRIMARY_WEAPON = 0, SECONDARY_WEAPON = 1;
	final int RESERVED_EQUIPMENT_SLOTS = 2;
	public static final int BASE_MOVE_FRAMES = 8;
	
	public ArrayList<ArrayList<Entity>> inventories;
	public ArrayList<Entity> inventory;
	public ArrayList<Entity> equipment;
	
	public Hardsuit hardsuit = null;
	
	public Actor(Vector2f position) {
		super(position);
		raycastable = true;
		damageable = true;
		targetable = true;
		pickupable = false;
		inventory = new ArrayList<Entity>();
		equipment = new ArrayList<Entity>();
		inventories = new ArrayList<ArrayList<Entity>>();
		inventories.add(inventory);
		inventories.add(equipment);
		raycastMask = Raycast.Mode.COVER.maskIndex;
		
		for (int i = 0; i < RESERVED_EQUIPMENT_SLOTS; i++) {
			equipment.add(null);
		}
	}
	
	@Override
	public void setPosition(Vector2f pos) {
		super.setPosition(pos);
		if (inventories != null) {
			for (ArrayList<Entity> inventory : inventories) {
				for(Entity entity : inventory) {
					if (entity != null) {
						entity.setPosition(pos);
					}
				}
			}
		}
	}
	
	@Override
	public void Damage(int damage) {
		Damage(damage, null);
	}
	
	@Override
	public void Damage(int damage, Vector2f damagePosition) {
		if (hardsuit == null) {
			super.Damage(damage, damagePosition);
		}
	}
	
	@Override
	public void Update(GameTime gameTime) {
		super.Update(gameTime);
		for(ArrayList<Entity> inv : inventories) {
			for (Entity e : inv) {
				if (e != null) {
					e.Update(gameTime);
				}
			}
		}
	}
	
	public void Attack(Vector2f originPosition, Vector2f targetPosition, ArrayList<Vector2f> cells) {
		Entity e = getPrimaryWeapon();
		if (e != null) {
			if (e instanceof Weapon) {
				Weapon w = (Weapon)e;
				if (w.canFire()) {
					w.fire(originPosition, targetPosition, cells, this);
					TakeTurn(w.actionFrames);
					return;
				} 
				
				if (w.magSize > 0 && w.magStatus == 0) {
					w.reload();
					TakeTurn(w.reloadTime);
					return;
				}
			} else {
				//attack with nonweapon entities
			}
		}
	}
	
	public Entity getPrimaryWeapon() {
		return inventories.get(EQUIPMENT).get(PRIMARY_WEAPON);
	}
	
	public Entity getSecondaryWeapon() {
		return inventories.get(EQUIPMENT).get(SECONDARY_WEAPON);
	}
	
	public Entity getInventoryItem(int index) {
		return inventory.get(index);
	}
	
	public Entity getEquipmentItem(int index) {
		return equipment.get(index);
	}
	
	public void setPrimaryWeapon(Entity w) {
		setEquipment(PRIMARY_WEAPON, w);
	}
	
	public void setSecondaryWeapon(Entity w) {
		setEquipment(SECONDARY_WEAPON, w);
	}
	
	public void setEquipment(int slot, Entity w) {
		inventories.get(EQUIPMENT).set(slot, w);
	}
	
	public Entity removeItemFromInventory(int index) {
		return inventory.remove(index);
	}
	
	public void addToInventory(Entity e) {
		inventory.add(e);
	}
	
	public Entity replaceInventoryItem(int index, Entity e) {
		Entity out = inventory.get(index);
		inventory.set(index, e);
		return out;
	}
	
	public boolean inventoryContainsItemInSlot(int column, int row) {
		return column >= 0 && row >= 0 && inventories.get(column).size() > row && inventories.get(column).get(row) != null;
	}
	
	public Entity getItemAtSlot(int column, int row) {
		return inventories.get(column).get(row);
	}
	
	public Entity throwItem(Vector2f originCell, Vector2f targetPosition, int column, int row) {
		Entity e = removeEntityFromInventory(column, row);
		e.throwEntity(originCell, targetPosition);
		return e;
	}

	private Entity removeEntityFromInventory(int column, int row) {
		Entity e;
		if (column == EQUIPMENT) {
			e = equipment.get(row);
			equipment.set(row, null);
		} else {
			e = inventories.get(column).remove(row);
		}
		return e;
	}
	
	public Entity dropItem(int column, int row) {
		Entity e = removeEntityFromInventory(column, row);
		InGame.entities.add(e);
		return e;
	}
	
	public void dropItem(Entity e) {
		for (int col = 0; col < inventories.size(); col++) {
			for (int row = 0; row < inventories.size(); row++) {
				if (getItemAtSlot(col, row).equals(e)) {
					removeEntityFromInventory(col, row);
				}
			}
		}
	}
	
	public void swapItemPositions(int i1, int i2) {
		Entity e1 = inventory.get(i1);
		inventory.set(i1, inventory.get(i2));
		inventory.set(i2, e1);
	}
	
	public void equipItem(int equipmentSlot, int inventorySlot) {
		Entity item = getInventoryItem(inventorySlot);
		setEquipment(equipmentSlot, item);
		removeItemFromInventory(inventorySlot);
	}
	
	public void addToEquipment(Entity e) {
		equipment.add(e);
	}
	
	public void unequipItem(int equipmentSlot) {
		Entity item = getEquipmentItem(equipmentSlot);
		if (item != null) {
			addToInventory(item);
			equipment.set(equipmentSlot, null);
		}
	}
	
	public boolean hasEquipmentInSlot(int slot) {
		return getEquipmentItem(slot) != null;
	}
}
