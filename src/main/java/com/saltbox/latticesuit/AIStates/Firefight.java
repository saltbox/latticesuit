package com.saltbox.latticesuit.AIStates;

import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.entities.actors.AIActor;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;

public class Firefight extends AIState {
	int noContactTicks = 0, ticksUntilReposition = 50, ticksUntilAssault = 150;
	public Firefight(AIActor actor) {
		super(actor);
	}

	@Override
	public void Start() {
		noContactTicks = 0;
	}

	@Override
	public void Update(GameTime gt) {
		actor.lookOutForPlayer();
		if (actor.noAlliesInFrontOfTarget()) {
			if (actor.getPrimaryWeapon() instanceof Weapon) {
				actor.attackWithWeapon(actor.targetPosition);
			}
			noContactTicks = 0;
		} else {
			noContactTicks++;
			if (noContactTicks > ticksUntilReposition) {
				if (actor.canSeeTarget()) {
					actor.setState("firefightreposition");
				}
			} 
			if (noContactTicks > ticksUntilAssault) {
				actor.setState("assault");
			}
		}
	}
}
