package com.saltbox.latticesuit;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.UI.CursorPositionReadout;
import com.saltbox.latticesuit.UI.PlayerUI;
import com.saltbox.latticesuit.UI.ShotDisplayer;
import com.saltbox.latticesuit.battlegroup.*;
import com.saltbox.latticesuit.databases.*;
import com.saltbox.latticesuit.entities.*;
import com.saltbox.latticesuit.entities.actors.*;
import com.saltbox.latticesuit.gamestates.GameState;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.Game;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.*;
import com.saltboxgames.latticelight.utils.*;

public class LatticeSuit extends Game {
	public static Vector2f halfSize;
	public static int fontSize = 12;
	
	public static boolean debugStart = true;
	
	public static LatticeSuit instance;
	
	public static GameState activeGameState;

	public static float repeatInputHangTime = 0.08f;
	FPSGameObject fps;
	CursorPositionReadout cursorPositionReadout;

	public LatticeSuit() {
		super("Hardsuit", new Vector2i(100, 50), 12);
		
		Directions.Set();
		instance = this;
		halfSize = new Vector2f((float) getSize().x / 2f, (float) getSize().y / 2f);
	}

	
	@Override
	public void Start() {
		DatabaseMediator.CreateDatabases();
		setTickRate(180);
		if (debugStart) {
			activeGameState = new InGame();
			activeGameState.Start();
		}
	}

	@Override
	public void Stop() {
		// TODO Auto-generated method stub
	}

	@Override
	public void LoadContent() {
		System.out.println("LOADCONTENT WORKS NOW");
	}

	@Override
	public void Update(GameTime gameTime) {
		activeGameState.Update(gameTime);
	}

	Vector2f playerPosition = new Vector2f();

	@Override
	public void Draw(SpriteBatch sb) {
		activeGameState.Draw(sb);
	}

	

	public static Vector2i GetSize() {
		return instance.getSize();
	}

	public static boolean isConsoleActive() {
		return instance.isLogEnabled();
	}
	
	
}
