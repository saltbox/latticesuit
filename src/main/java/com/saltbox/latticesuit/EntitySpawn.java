package com.saltbox.latticesuit;

import java.io.Externalizable;

import org.joml.Vector2f;
import org.joml.Vector2i;

public abstract class EntitySpawn {
	public Vector2i position;
	protected String name;
	protected String spawnType;
	
	public EntitySpawn(Vector2i position, String name) {
		this.position = position;
		this.name = name;
		
	}
	
	public abstract void spawn();
	
	public String getName() {
		return name;
	}
	
	public String getSpawnType() {
		return spawnType;
	}
	
	public Vector2i getPosition() {
		return position;
	}
}
