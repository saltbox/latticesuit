package com.saltbox.latticesuit;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;

import com.saltbox.latticesuit.entities.Entity;

class RaycastState {
	public RaycastState(Vector2f origin, Vector2f target, int modeMask, int maxRange, int damage, int castCount) {
		deltaX = Math.abs(target.x - origin.x);
		deltaY = Math.abs(target.y - origin.y);
		xStep = (target.x >= origin.x) ? 1 : -1;
		yStep = (target.y >= origin.y) ? 1 : -1;
		error = deltaX - deltaY;
		xGrid = origin.x; 
		yGrid = origin.y;
		this.modeMask = modeMask;
		this.damage = damage;
		this.castCount = castCount;
		this.maxRange = maxRange;
		this.castSteps = 0;
	}
	
	public float deltaX, deltaY, xStep, yStep, error, xGrid, yGrid;
	public int castSteps, modeMask;
	public int maxRange, damage, castCount;
}
