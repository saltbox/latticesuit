package com.saltbox.latticesuit.playercommands.mapediting;

import static org.lwjgl.glfw.GLFW.*;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.TilePool;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.playercommands.PlayerCommand;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class DrawSquare extends PlayerCommand {

	public DrawSquare(Player player) {
		super(player);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(SpriteBatch sb) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	Vector2i squareV1, squareV2 = new Vector2i(), squareDelta = new Vector2i(), zero = new Vector2i();
	Vector2i targetCell = new Vector2i();
	TilePool pool = new TilePool('*', new Vector3f(0, 1, 0));

	@Override
	public void update(GameTime gameTime) {
		if (MouseInput.isButtonDown(0) && squareV1 == null) {
			squareV1 = new Vector2i();
			InGame.map.getMousePosInMap(squareV1);
			return;
		}

		if (squareV1 != null) {
			if (!MouseInput.isButtonDown(0)) {
				InGame.map.getMousePosInMap(squareV2);
				squareDelta.set(squareV2).sub(squareV1);

				if (squareDelta.x != 0) {
					squareDelta.x = squareDelta.x / Math.abs(squareDelta.x);
				} else {
					squareDelta.x = 0;
				}

				if (squareDelta.y != 0) {
					squareDelta.y = squareDelta.y / Math.abs(squareDelta.y);
				} else {
					squareDelta.y = 0;
				}

				int safeguard = (Math.abs(squareV2.x) + 1) * (Math.abs(squareV2.y) + 1) * (Math.abs(squareV1.x) + 1)
						* (Math.abs(squareV1.y) + 1);
				int safeCounter = 0;

				for (int x = squareV1.x; x != squareV2.x + squareDelta.x || squareDelta.x == 0; x += squareDelta.x) {
					for (int y = squareV1.y; y != squareV2.y + squareDelta.y
							|| squareDelta.y == 0; y += squareDelta.y) {
						if (x == squareV1.x || x == squareV2.x || y == squareV1.y || y == squareV2.y) {
							targetCell.set(x, y);
							InGame.map.SetTileType(targetCell, ((EditMap)player.commands.get("editmap")).getSelectedTileType());
							
							safeCounter++;
							if (safeCounter > safeguard) {
								squareV1 = null;
								return;
							}
						}
					}
				}
				squareV1 = null;
				return;

			}

		}

		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("editmap");
		}

	}

	@Override
	public void activate() {
		// TODO Auto-generated method stub

	}

}
