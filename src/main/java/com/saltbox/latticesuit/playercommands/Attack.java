package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.TilePool;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Attack extends PlayerCommand {
	public Entity target;
	
	public boolean confirmAttack = false;
	public boolean showShotDistribution = true;
	public boolean throwMode = false;
	int throwTime = 6;
	int throwDamage = 5;
	int selectedEquipmentSlot = 0;
	Vector2f originCell;
	Vector2f previousCursorPosition;
	
	SelectItemFromInventory selectInventory;

	public Attack(Player player) {
		super(player);
		name = "attack";
	}

	@Override
	public void draw(SpriteBatch sb) {
		
	}

	Vector2f targetPosition = new Vector2f(), cursorPosition = new Vector2f();
	ArrayList<Vector2f> hitTiles = new ArrayList<Vector2f>();

	@Override
	public boolean doCommand() {
		if (confirmAttack) {
			if (showShotDistribution) {
				hitTiles = new ArrayList<Vector2f>();
			} else {
				hitTiles = null;
			}

			player.cursor.getPosition(cursorPosition);
			
			if (throwMode) {
				throwSelectedItem(originCell, cursorPosition);
			} else {
				player.Attack(originCell, cursorPosition, hitTiles, selectedEquipmentSlot);
			}

			if (showShotDistribution && hitTiles.size() > 0) {
				switchToShotDistributionDisplay();
			} else {
				player.activateCommandMode("default");
			}

			target = null;
			confirmAttack = false;
			throwMode = false;
			return true;
		}
		return false;
	}

	private void switchToShotDistributionDisplay() {
		((ShotDistributionDisplay) player.commands.get("shotdistributiondisplay")).hitPositions
				.addAll(hitTiles);
		player.activateCommandMode("shotdistributiondisplay");
	}

	private void throwSelectedItem(Vector2f originCell, Vector2f cursorPosition) {
		player.throwItem(originCell, cursorPosition, selectInventory.selectedInventory, selectInventory.getSelectedItemRow());
		player.TakeTurnAndPeek(player.throwFrames, originCell);
	}

	Vector2f zero = new Vector2f();
	boolean firstFrameActive;
	ArrayList<Vector2f> hitCells = new ArrayList<Vector2f>();
	@Override
	public void update(GameTime gameTime) {
		if (!player.directionInput.equals(zero)) {
			player.cursor.Move(player.directionInput);
			player.inputPause();
		}
		previousCursorPosition = player.cursor.getPosition();
		if (MouseInput.isButtonDown(0)) {
			player.cursor.setPosition(player.mousePosition);
		}
		
		if (previousCursorPosition == null || previousCursorPosition.equals(player.cursor.getPosition())) {
			if (isCursorVisibleFrom(player.getPosition())) {
				originCell = new Vector2f(player.getPosition());
				player.cursor.setColor(new Vector3f(0, 1, 0));
			} else {
				ArrayList<Vector2f> cells = InGame.map.getPathableNeighbors(player.getPosition(), false);
				for (int i = 0; i < cells.size(); i++) {
					if (!isCursorVisibleFrom(cells.get(i))) {
						cells.remove(i);
						i--;
					}
				}
				
				Vector2f cellClosest = null;
				for (Vector2f cell : cells) {
					if (cellClosest == null || player.cursor.getPosition().distance(cell) < player.cursor.getPosition().distance(cellClosest)) {
						cellClosest = cell;
					}
				}
				
				if (cellClosest != null) {
					originCell = cellClosest;
					player.cursor.setColor(new Vector3f(0, 1, 0));
				} else {
					originCell = player.getPosition();
					player.cursor.setColor(new Vector3f(0, 0, 1));
				}
			}
		}

		if (KeyboardInputTools.confirm()) {
			for (Entity entity : InGame.targetableEntities) {
				if (entity.getPosition().equals(player.cursor.getPosition())) {
					target = entity;
				}
			}
			confirmAttack = true;
			player.cursor.active = false;
		}

		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
			player.cursor.active = false;
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_T)) {
			player.activateCommandMode("selectitemfrominventory");
			selectInventory.playerCommandToReturnTo = this.name;
			throwMode = true;
		}
		
		if (throwMode && selectInventory.confirmSelection) {
			confirmAttack = true;
			player.cursor.active = false;
		}
		
		for (int i = 0; i < player.equipment.size() && i <= GLFW_KEY_9 ; i++) {
			if (KeyboardInput.isKeyDown(i + GLFW_KEY_1) && !KeyboardInput.isKeyDown(i + GLFW_KEY_1, true)) {
				selectedEquipmentSlot = i;
			}
		}

		if (KeyboardInput.isKeyDown(GLFW_KEY_A) && !KeyboardInput.isKeyDown(GLFW_KEY_A, true) && !firstFrameActive) {
			if (KeyboardInput.isKeyDown(GLFW_KEY_LEFT_SHIFT)) {
				player.activateCommandMode("selecttarget");
			} else {
				confirmAttack = true;
				player.cursor.active = false;
			}
		}
		firstFrameActive = false;
	}
	
	private boolean isCursorVisibleFrom(Vector2f cell) {
		hitCells.clear();
		Raycast.raycast(cell, player.cursor.getPosition(), hitCells, player);
		return hitCells.contains(player.cursor.getPosition());
	}

	@Override
	public void activate() {
		selectInventory = (SelectItemFromInventory)player.commands.get("selectitemfrominventory");
		player.cursor.active = true;
		previousCursorPosition = null;
		firstFrameActive = true;
		if (target == null) {
			if (!throwMode) {
				player.cursor.setPosition(player.getPosition());
			}
			Entity closestTargetable = findClosestTargetableEntity();
			if (closestTargetable != null) {
				targetEntity(closestTargetable);
			}
			if (KeyboardInput.isKeyDown(GLFW_KEY_LEFT_SHIFT)) {
				player.activateCommandMode("selecttarget");
			}
		}
	}

	private void targetEntity(Entity closestTargetable) {
		player.cursor.setPosition(closestTargetable.getPosition());
		target = closestTargetable;
	}

	Vector2f playerPos = new Vector2f();

	private Entity findClosestTargetableEntity() {
		Entity closestTargetable = player;
		player.getPosition(playerPos);
		for (Entity e : InGame.targetableEntities) {
			if (e != player) {
				if (e.getPosition().distance(playerPos) < closestTargetable.getPosition().distance(playerPos)) {
					RaycastHit hit = Raycast.raycast(playerPos, e.getPosition(), null, player); 
					if (hit.hitEntity != null) {
						closestTargetable = e;
					}
				}
			}
		}
		return closestTargetable;
	}
}
