package com.saltbox.latticesuit;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.battlegroup.Fireteam;
import com.saltbox.latticesuit.battlegroup.IFireteamMember;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltbox.latticesuit.gamestates.InGame;

public class ActorSpawn extends EntitySpawn {
	public int fireteamNum = -1;
	
	public ActorSpawn(Vector2i position, String name, int fireteam) {
		super(position, name);
		spawnType = "actor";
		this.fireteamNum = fireteam;	
	}
	
	public ActorSpawn(Vector2i position, String name) {
		this(position, name, -1);
	}

	@Override
	public void spawn() {
		Actor newActor = DatabaseMediator.actors.CreateActor(name, new Vector2f(position.x, position.y));
		InGame.addEntityToList(newActor);
		if (fireteamNum >= 0 && newActor instanceof IFireteamMember) {
			Fireteam.AddMemberToFireteam((IFireteamMember)newActor, fireteamNum);
			((IFireteamMember)newActor).setFireteam(Fireteam.getFireteam(fireteamNum));
		}
	}
}
