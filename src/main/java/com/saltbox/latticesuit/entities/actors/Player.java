package com.saltbox.latticesuit.entities.actors;

import java.util.ArrayList;
import java.util.HashMap;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.*;
import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.databases.ItemDatabase;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.items.SmokeGrenade;
import com.saltbox.latticesuit.items.Weapon;
import com.saltbox.latticesuit.playercommands.*;
import com.saltbox.latticesuit.playercommands.mapediting.*;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Player extends Actor {
	public int framesSinceStart;
	public float hangTime;
	public String commandMode = "default", previousCommandMode = "default";
	public CommandTile cursor;
	public HashMap<String, PlayerCommand> commands = new HashMap<String, PlayerCommand>();
	
	public final int moveFrames = Actor.BASE_MOVE_FRAMES, sprintFrames= Actor.BASE_MOVE_FRAMES / 4, throwFrames = 5;
	public boolean adrenalineMode = false;

	public Vector2f directionInput = new Vector2f(0, 0);
	public Vector2f mousePosition = new Vector2f();
	public boolean cameraLock = true;
	Vector2f lastFrameDirectionInput = new Vector2f(0, 0);
	
	public Player() {
		super(new Vector2f(0, 0));
		setupPlayerVariables();
		setupCommandModes();
		hardsuit = new Hardsuit();
	}

	public void setupPlayerVariables() {
		setSprite('@');
		cursor = new CommandTile(getPosition(), 'X', new Vector3f(0, 0.2f, 1f));
		health = 100;
		damageable = true;
		targetable = true;
		raycastable = true;
		name = "player";
		raycastMask = raycastMask | Raycast.Mode.PLAYER.maskIndex;
		setPrimaryWeapon((Weapon) DatabaseMediator.items.CreateItem("Weapon", "Pistol", getPosition()));
	}

	public void setupCommandModes() {
		commands.put("sprint", new Sprint(this));
		commands.put("attack", new Attack(this));
		commands.put("flash", new Flash(this));
		commands.put("selecttarget", new SelectTarget(this));
		commands.put("shotdistributiondisplay", new ShotDistributionDisplay(this));
		commands.put("selectitemfrominventory", new SelectItemFromInventory(this));
		commands.put("get", new Get(this));
		commands.put("equip", new Equip(this));
		commands.put("interact", new Interact(this));
		commands.put("lookaround", new LookAround(this));
		commands.put("history", new History(this));
		commands.put("drop", new Drop(this));
		commands.put("plant", new Plant(this));

		commands.put("default", new DefaultPlayerCommand(this));
		
		commands.put("editmap", new EditMap(this));
		commands.put("drawsquare", new DrawSquare(this));
		commands.put("selectspawnflag", new SelectSpawnFlag(this));
		
		commands.put("debugdata", new DebugData(this));
	}

	
	@Override
	public void Update(GameTime gameTime) {
		lastFrameDirectionInput = directionInput;
		directionInput = checkDirectionInput();
		mousePosition = checkMouseInput();
		
		if (health <= 0) {
			if (!commandMode.equals("history")) {
				GameLog.writeLine("you're dead. idiot.");
			}
			activateCommandMode("history");
		}
		
		if (waitforHangTime((float) gameTime.getDeltaTime()) && actionFrames <= 0) {
			return;
		}
		if (actionFrames > 0) {
			framesSinceStart++;
			super.Update(gameTime);
			if (adrenalineMode && actionFrames > 0) {
				actionFrames = 0;
			}
			return;
		}
		
		

		if (actionFrames <= 0) {
			boolean commandTakingTurn;
			for (String key : commands.keySet()) {
				if (!LatticeSuit.isConsoleActive()) {
					if (commandMode.equals(key)) {
						commands.get(key).update(gameTime);
					}
				}

				commandTakingTurn = commands.get(key).doCommand();
				if (commandTakingTurn) {
					return;
				}
			}
		}
	}

	Vector2f targetCameraPosition = new Vector2f();
	private boolean waitforHangTime(float gameTime) {
		if (hangTime > 0) {
			if (!directionInput.equals(lastFrameDirectionInput)) {
				hangTime = 0;
			} else {
				hangTime -= gameTime;
				return true;
			}
		}
		return false;
	}

	@Override
	public void Draw(SpriteBatch sb) {
		if (cameraLock) {
			getPosition(targetCameraPosition);
			Camera.Main.setPosition(targetCameraPosition.sub(LatticeSuit.halfSize));
		}
		cursor.Draw(sb);
		super.Draw(sb);
		for (String key : commands.keySet()) {
			if (commandMode.equals(key)) {
				commands.get(key).draw(sb);
			}
		}
	}
	
	@Override
	public void Damage(int damage) {
		if (hardsuit == null) {
			damageWithoutHardsuit(damage);
		} else {
			damageWithHardsuit(damage);
		}
	}

	private void damageWithHardsuit(int damage) {
		ComponentHit hit = hardsuit.Damage(damage);
		if (damage > 0) {
			if (hit.componentWasAlreadyDestroyed) {
				GameLog.writeLine("Your body is hit for " + damage + " damage");
				health -= damage;
			} else {
				GameLog.writeLine("Your " + hit.name + " is hit for " + damage + " damage");
				if (hit.componentIsDestroyed) {
					GameLog.writeLine("Your " + hit.name + " is destroyed");
				}
			}
		} else {
			if (hit.componentWasAlreadyDestroyed) {
				GameLog.writeLine("A shot barely misses you");
			} else {
				GameLog.writeLine("A shot glances off your " + hit.name);
			}
		}
	}

	private void damageWithoutHardsuit(int damage) {
		if (damage > 0) {
			GameLog.writeLine("You're hit for " + damage + " damage");
			health -= damage;
		} else {
			GameLog.writeLine("A round barely misses you");
		}
	}
	
	Vector2f checkDirectionInput() {
		for (int key : Directions.map.keySet()) {
			if (KeyboardInput.isKeyDown(key)) {
				return new Vector2f(Directions.Get(key));
			}
		}
		return new Vector2f(0, 0);
	}
	Vector2f mouseOut = new Vector2f();
	Vector2f checkMouseInput() {
		InGame.map.getMousePosInMap(mouseOut);
		return mouseOut;
	}

	public void inputPause() {
		inputPause(LatticeSuit.repeatInputHangTime);
	}
	
	public void inputPause(float time) {
		hangTime = time;
	}

	public void activateCommandMode(String mode) {
		if (commands.containsKey(mode)) {
			previousCommandMode = commandMode;
			Logger.writeLine("command mode activated: " + mode);
			commands.get(mode).activate();
			commandMode = mode;
		} else {
			Logger.writeLine("command mode not registered: " + mode);
		}
	}
	
	@Override 
	public void Attack(Vector2f originPosition, Vector2f targetPosition, ArrayList<Vector2f> cells) {
		Attack(originPosition, targetPosition, cells, 0);
	}
	
	public void Attack(Vector2f originPosition, Vector2f targetPosition, ArrayList<Vector2f> cells, int equipmentSlot) {
		Entity e = equipment.get(equipmentSlot);
		if (e != null) {
			if (e.health <= 0) {
				GameLog.writeLine("The " + e.getName() + " is destroyed");
			} else {
				if (e instanceof Weapon) {
					Weapon w = (Weapon)e;
					if (w.canFire()) {
						w.fire(originPosition, targetPosition, cells, this);
						TakeTurnAndPeek(w.useTime, originPosition);
						GameLog.writeLine("You attack with the " + w.getName());
						return;
					} 
					
					if (w.magSize > 0 && w.magStatus == 0) {
						w.reload();
						TakeTurn(w.reloadTime);
						GameLog.writeLine("You reload the " + w.getName());
						return;
					}
				} else {
					RaycastHit hit = Raycast.meleeRaycast(originPosition, targetPosition, cells, this, 3, e.meleeDamage);
					if (hit != null && hit.hitEntity != null && e.selfDamageOnMelee) {
						e.Damage(e.meleeDamage);
					}
					GameLog.writeLine("You attack with the " + e.getName());
					TakeTurn(e.meleeFrames);
				}
			}
		}
	}
	
	@Override
	public void TakeTurnAndPeek(Integer frames, Vector2f peekCell) {
		super.TakeTurnAndPeek(frames, peekCell);
		cameraLock = false;
	}
	
	@Override
	protected void onReturnFromPeek() {
		super.onReturnFromPeek();
		cameraLock = true;
	}
	
	public void getMouseInputDirection(Vector2f v) {
		Vector2f cameraPosition = new Vector2f();
		Camera.Main.getPosition(cameraPosition);
		
		v.set(mousePosition);
		v.sub(cameraPosition);
		v.sub(LatticeSuit.halfSize);
		v.normalize();
		v.x = Math.round(v.x);
		v.y = Math.round(v.y);
	}
}