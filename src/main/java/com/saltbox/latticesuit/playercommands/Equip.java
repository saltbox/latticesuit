package com.saltbox.latticesuit.playercommands;

import java.util.ArrayList;

import org.joml.Vector2f;

import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class Equip extends InventoryManagementCommand {
	Menu inventoryMenu;
	Menu equipmentMenu;
	
	public Equip(Player player) {
		super(player);
		name = "equip";
		addMenu(player.inventory);
		addMenu(player.equipment);
		equipmentMenu = menus.get(1);
		inventoryMenu = menus.get(0);
		maintainPositionInInactiveMenus = true;
	}

	@Override
	public void activate() {
		autoUpdateMenuContents();
		inventoryMenu.selected = true;
	}
	
	@Override
	public void autoUpdateMenuContents() {
		super.autoUpdateMenuContents();
		if (inventoryMenu.options.size() == 0) {
			inventoryMenu.options.add("Nothing in inventory");
		}
		
		if (player.getPrimaryWeapon() == null) {
			equipmentMenu.options.add(0, "No primary weapon");
		}
		
		if (player.getSecondaryWeapon() == null) {
			equipmentMenu.options.add(1, "No secondary weapon");
		}
	}
	
	@Override
	public void confirmSelection() {
		super.confirmSelection();
		if (selectedInventory == 0) {
			if (player.hasEquipmentInSlot(equipmentMenu.getSelectedOption())) {
				player.unequipItem(equipmentMenu.getSelectedOption());
			} else {
				player.equipItem(equipmentMenu.getSelectedOption(), inventoryMenu.getSelectedOption());
				inventoryMenu.setSelectedOption(Math.max(0, inventoryMenu.getSelectedOption() - 1));
			}
			
		} else if (selectedInventory == 1) {
			player.unequipItem(equipmentMenu.getSelectedOption());
			if (!player.hasEquipmentInSlot(equipmentMenu.getSelectedOption())) {
				player.equipItem(equipmentMenu.getSelectedOption(), inventoryMenu.getSelectedOption());
				inventoryMenu.setSelectedOption(Math.max(0, inventoryMenu.getSelectedOption() - 1));
			}
		}
		autoUpdateMenuContents();
	}
	
	
}
