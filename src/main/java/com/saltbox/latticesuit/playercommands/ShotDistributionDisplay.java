package com.saltbox.latticesuit.playercommands;

import java.util.ArrayList;
import static org.lwjgl.glfw.GLFW.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.entities.TilePool;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.Camera;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class ShotDistributionDisplay extends PlayerCommand {
	public ArrayList<Vector2f> hitPositions = new ArrayList<Vector2f>();
	public HashMap<Vector2f, Integer> hitPositionCounts = new HashMap<Vector2f, Integer>();
	public TilePool hitTilePool;
	public Vector2f peekReturnCell;

	public ShotDistributionDisplay(Player player) {
		super(player);
		name = "shotdistributiondisplay";
		hitTilePool = new TilePool('1', new Vector3f(0, 1, 0));
	}

	@Override
	public void draw(SpriteBatch sb) {
		for (int i = 0; i < hitTilePool.size(); i++) {
			hitTilePool.get(i).Draw(sb);
		}
	}

	@Override
	public boolean doCommand() {

		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		if (KeyboardInputTools.confirm() || KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
			hitPositions.clear();
			hitPositionCounts.clear();
			if (peekReturnCell != null) {
				player.setPosition(peekReturnCell);
				peekReturnCell = null;
			}
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_A) && !KeyboardInput.isKeyDown(GLFW_KEY_A, true)) {
			player.activateCommandMode("attack");
			hitPositions.clear();
			hitPositionCounts.clear();
			if (peekReturnCell != null) {
				player.setPosition(peekReturnCell);
				peekReturnCell = null;
			}
		}
	}

	@Override
	public void activate() {
		if (player.peekReturnCell != null) {
			peekReturnCell = new Vector2f(player.peekReturnCell);
			player.peekReturnCell = null;
		}
		hitTilePool.hideAll();
		for (Vector2f p : hitPositions) {
			if (hitPositionCounts.get(p) == null) {
				hitPositionCounts.put(new Vector2f(p), 1);
			} else {
				hitPositionCounts.put(new Vector2f(p), hitPositionCounts.get(p) + 1);
			}
		}

		Iterator<Vector2f> keys = hitPositionCounts.keySet().iterator();
		Vector2i zero = new Vector2i();
		int tileIndex = 0;
		while (keys.hasNext()) {
			tileIndex++;
			Vector2f key = keys.next();
			int count = hitPositionCounts.get(key);
			if (count > 9) {
				hitTilePool.get(tileIndex).setSprite('+');
				hitTilePool.get(tileIndex).setColor(zero, new Vector3f(1, 0, 0));
			} else {
				hitTilePool.get(tileIndex).setSprite(Integer.toString(count).charAt(0));
				hitTilePool.get(tileIndex).setColor(zero, new Vector3f(count / 9f, 1 - count / 9f, 0));
			}
			hitTilePool.get(tileIndex).setPosition(key);
			hitTilePool.get(tileIndex).active = true;
		}
	}

}
