package com.saltbox.latticesuit.AIStates;

import com.saltbox.latticesuit.entities.actors.AIActor;
import com.saltbox.latticesuit.entities.actors.Actor;
import com.saltboxgames.latticelight.GameTime;

public abstract class AIState {
	AIActor actor;
	public AIState(AIActor actor) {
		this.actor = actor;
	}
	
	public abstract void Start();
	
	public abstract void Update(GameTime gt);
}
