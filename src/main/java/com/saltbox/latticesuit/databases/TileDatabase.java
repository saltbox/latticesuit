package com.saltbox.latticesuit.databases;

import java.util.ArrayList;
import java.util.HashMap;

import org.joml.Vector2i;

public class TileDatabase extends Database {
	TileDefinition[] tileDefinitions;
	public TileDatabase() {
		super("Data/Definitions/tiles.csv");
		createTileDefinitions(firstEntry);
	}
	
	protected void createTileDefinitions(HashMap<String, HashMap<String, String>> firstEntry) {
		tileDefinitions = new TileDefinition[firstEntry.keySet().size()];
		int i = 0;
		for (String key : firstEntry.keySet()) {
			tileDefinitions[i] = new TileDefinition(key, firstEntry);
			i++;
		}
	}
	
	public TileDefinition getTileDefinition(int type) {
		return tileDefinitions[type];
	}
	
	public ArrayList<Integer> getTileDefinitions() {
		ArrayList<Integer> tiles = new ArrayList<Integer>();
		for (TileDefinition tile : tileDefinitions) {
			tiles.add(tile.type);
		}
		return tiles;
	}
}
