package com.saltbox.latticesuit.entities;

import java.util.Random;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameTime;

public class SmokeCloud extends Entity {
	static Random rng = new Random(); 
	int lifetime = 400;
	Vector2i spritePos = new Vector2i(2, 11);
	public SmokeCloud(Vector2f position) {
		super(position);
		raycastMask = raycastMask | Raycast.Mode.VISION.maskIndex;
		spritePos.x = Entity.spriteRandom.nextInt(3);
		setSprite(spritePos);
		raycastable = true;
		pickupable = false;
		lifetime += rng.nextInt(500);
		//moveToEmptyCell();
	}

	private void moveToEmptyCell() {
		for (int i = 0; i < 30; i++) {
			for (Entity e : InGame.entities) {
				if (e.getPosition().equals(getPosition()) && this.getClass().isInstance(e)) {
					setPosition(new Vector2f(getPosition()).add(new Vector2f(rng.nextInt(3) - 1, rng.nextInt(3) - 1)));
					break;
				}
			}
		}
	}

	Vector2f randomVector = new Vector2f();
	
	@Override
	public void Update(GameTime gt) {
		super.Update(gt);
		lifetime--;
		if (lifetime <= 0) {
			InGame.removeEntityFromList(this);
		}
		if (Entity.spriteRandom.nextFloat() > 0.98f) {
			randomVector.set(Entity.spriteRandom.nextInt(3) - 1, Entity.spriteRandom.nextInt(3) - 1);
			Move(randomVector);
		}
	}
}
