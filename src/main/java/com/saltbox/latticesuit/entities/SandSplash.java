package com.saltbox.latticesuit.entities;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltboxgames.latticelight.GameObject;
import com.saltboxgames.latticelight.GameTime;

public class SandSplash extends Entity {
	int lifeTime = 60;
	Vector2i spritePos = new Vector2i(2, 11);

	public SandSplash(Vector2f position) {
		super(position, 1);
		setSprite(",.'".charAt(Entity.spriteRandom.nextInt(3)));
		setColor(new Vector2i(0, 0), new Vector3f(1, 1, 1));
		pickupable = false;
	}

	Vector2f randomVector = new Vector2f();

	@Override
	public void Update(GameTime gameTime) {
		super.Update(gameTime);
		lifeTime--;
		if (lifeTime % 15 == 0) {
			if (lifeTime > 15) {
				spritePos.x = Entity.spriteRandom.nextInt(3);
				setSprite(spritePos);
			} else {
				setSprite(",.'".charAt(spriteRandom.nextInt(3)));
			}
			if (lifeTime == 30) {
				if (Entity.spriteRandom.nextBoolean()) {
					randomVector.set(Entity.spriteRandom.nextInt(3) - 1, Entity.spriteRandom.nextInt(3) - 1);
					Move(randomVector);
				}
			}
		}

		if (lifeTime <= 0) {
			InGame.entities.remove(this);
		}
	}
}
