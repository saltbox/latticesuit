package com.saltbox.latticesuit.items;

import java.util.ArrayList;
import java.util.Random;

import org.joml.Vector2f;

import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.gamestates.InGame;

public class FragGrenade extends Grenade {
	static Random rng = new Random();
	int fragments = 100;
	int fragmentDamage = 20;
	public FragGrenade(Vector2f position) {
		super(position);
	}
	
	@Override
	public void goOff() {
		super.goOff();
		Vector2f target = new Vector2f();
		for (int i = 0; i < fragments; i++) {
			target.x = rng.nextInt(50) - 25;
			target.y = rng.nextInt(50) - 25;
			while (target.equals(getPosition())) {
				target.x = rng.nextInt(50) - 25;
				target.y = rng.nextInt(50) - 25;
			}
			ArrayList<Vector2f> cells = new ArrayList<Vector2f>();
			Raycast.bulletRaycast(getPosition(), target.add(getPosition()), cells, this, fragmentDamage);
			InGame.shotDisplayer.addNewShot(cells, this);
		}
		Damage(health);
	}

}
