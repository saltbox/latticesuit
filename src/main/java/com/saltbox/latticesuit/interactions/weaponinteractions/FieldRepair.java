package com.saltbox.latticesuit.interactions.weaponinteractions;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.interactions.Interaction;
import com.saltbox.latticesuit.items.Weapon;
import com.saltbox.latticesuit.items.Weapon;

public class FieldRepair extends Interaction {
	Weapon weapon;
	public FieldRepair(Weapon w) {
		super("field repair");
		weapon = w;
	}

	@Override
	public void doInteraction() {
		if (weapon.health < weapon.maxHealth) {
			weapon.health += weapon.maxHealth / 2;
			weapon.health = Math.min(weapon.health, weapon.maxHealth);
			GameLog.writeLine("You repair the " + weapon.getName());
		} else {
			GameLog.writeLine("The " + weapon.getName() + " can't be repaired any further");
		}
	}

	@Override
	public int getFrames() {
		if (weapon.health < weapon.maxHealth) {
			return weapon.fieldRepairTime;
		}
		return 0;
	}

}
