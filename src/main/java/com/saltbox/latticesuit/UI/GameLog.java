package com.saltbox.latticesuit.UI;

import org.joml.Vector2i;
import org.joml.Vector3f;

public class GameLog {
	public static Textbox playerLog, playerLogLarge;
	
	public static void writeLine(String text) {
		playerLog.updateTextAsLog(text);
		playerLogLarge.updateTextAsLog(text);
	}
	
	public static void expand() {
		PlayerUI.historyLarge = true;
	}
	
	public static void retract() {
		PlayerUI.historyLarge = false;
	}
}
