package com.saltbox.latticesuit.items;

import java.util.ArrayList;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.interactions.Interactions;

public class Weapon extends Entity {
	public int damage = 1, useTime = 4;
	
	public int magSize, magStatus, shotsPerAttack, reloadTime, internalsTestTime, 
	fieldRepairTime, checkAmmoTime, projectilesPerShot;
	public float accuracy;
	
	Vector2f adjustedTargetPosition = new Vector2f(), randomVector = new Vector2f();

	public Weapon(Vector2f position) {
		super(position, 0.8f);
		useTime = 10;
		magSize = 30;
		magStatus = magSize;
		damage = 4;
		shotsPerAttack = 10;
		projectilesPerShot = 1;
		damageable = true;
		health = 10;
		maxHealth = 10;
		reloadTime = 16;
		accuracy = 1;
		internalsTestTime = 4;
		fieldRepairTime = 24;
		checkAmmoTime = 1;
		setSprite('W');
		
		interactions.addAll(Interactions.firearmSet(this));
	}

	public void fire(Vector2f originPosition, Vector2f targetPosition, ArrayList<Vector2f> shotCells, Entity owner) {
		for (int i = 0; i < shotsPerAttack; i++) {
			if (magStatus > 0) {
				magStatus--;
				for (int j = 0; j < projectilesPerShot; j++) {
					adjustedTargetPosition.set(targetPosition);
					randomVector.set(Entity.attackRandom.nextFloat() - 0.5f, Entity.attackRandom.nextFloat() - 0.5f);
					adjustedTargetPosition.sub(randomVector.mul(5 / accuracy * 0.12f));
					RaycastHit hit;
					if (shotCells != null) {
						hit = Raycast.bulletRaycast(originPosition, adjustedTargetPosition, shotCells, owner, damage);
					} else {
						hit = Raycast.bulletRaycast(originPosition, adjustedTargetPosition, null, owner, damage);
					}
					if (hit != null && hit.hitEntity != null) {
						hit.hitEntity.Damage(damage, hit.hitPosition);
					}
				}
			}
		}
	}
	
	public void fire(Vector2f origin, Vector2f direction) {
		fire(origin, direction, null, null);
	}
	
	public boolean canFire() {
		return magSize <= 0 || magStatus > 0;
	}
	
	public String getCondition() {
		return DatabaseMediator.strings.getWeaponInternalConditionString(health, maxHealth);
	}
	
	public String getAmmoStatus() {
		return DatabaseMediator.strings.getWeaponAmmoStatusString(magStatus, magSize);
	}
	
	public String getShortAmmoStatus() {
		return magStatus + "/" + magSize;
	}
	
	public void reload() {
		magStatus = magSize;
	}
}
