package com.saltbox.latticesuit.playercommands.mapediting;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import com.saltbox.latticesuit.ActorSpawn;
import com.saltbox.latticesuit.CommandTile;
import com.saltbox.latticesuit.Directions;
import com.saltbox.latticesuit.EntitySpawn;
import com.saltbox.latticesuit.ItemSpawn;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.PlayerSpawn;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.battlegroup.IFireteamMember;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.entities.TilePool;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.playercommands.PlayerCommand;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.UIGameObject;
import com.saltboxgames.latticelight.input.*;
import com.saltboxgames.latticelight.rendering.*;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

public class EditMap extends PlayerCommand {
	public String entityToSpawn = "targetdummy";
	public int squad = 1;
	public boolean spawnItem = false;
	public ArrayList<Integer> tileTypes;
	public int selectedTileTypeIndex;
	public Menu tileSelectionDisplay;
	
	TilePool flagPool;
	public EditMap(Player player) {
		super(player);
		flagPool = new TilePool('f', new Vector3f(1, 0, 0));
		tileTypes = DatabaseMediator.tiles.getTileDefinitions();
		tileSelectionDisplay = new Menu(new Vector2f(0, 1), new Vector2i(tileTypes.size(), 1));
		tileSelectionDisplay.horizontal = true;
		tileSelectionDisplay.spriteMode = true;
		for (Integer i : tileTypes) {
			tileSelectionDisplay.spriteOptions.add(DatabaseMediator.tiles.getTileDefinition(i.intValue()).getSprites()[0]);
		}
	}

	@Override
	public void draw(SpriteBatch sb) {
		tileSelectionDisplay.Draw(sb);
		for (int i = 0; i < InGame.map.spawns.size(); i++) {
			EntitySpawn s = InGame.map.spawns.get(i);
			CommandTile tile = flagPool.get(i);
			setTileCharacter(s, tile);
			flagPool.get(i).setPosition(s.getPosition().x, s.getPosition().y);
			flagPool.get(i).active = true;
			flagPool.get(i).setZDepth(4f);
		}
		flagPool.Draw(sb);
	}

	private static void setTileCharacter(EntitySpawn s, CommandTile tile) {
		if (s instanceof ActorSpawn) {
			ActorSpawn a = (ActorSpawn)s;
			if (a.getName().equals("player")) {
				tile.setSprite('P');
			} else if (a.fireteamNum >= 0) {
				if (a.fireteamNum < 10) {
					tile.setSprite(String.valueOf(a.fireteamNum).charAt(0));
				} else {
					tile.setSprite('+');
				}
			} else {
				tile.setSprite('A');
			}
		} else if (s instanceof ItemSpawn) {
			tile.setSprite('I');
		}
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	Vector2i targetCell = new Vector2i();

	@Override
	public void update(GameTime gameTime) {
		Vector2f cameraPosition = new Vector2f();
		Camera.Main.getPosition(cameraPosition);
		if (checkDirectionInput().x != 0 || checkDirectionInput().y != 0) {
			player.inputPause();
		}
		cameraPosition.add(checkDirectionInput());
		Camera.Main.setPosition(cameraPosition);
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_RIGHT) && !KeyboardInput.isKeyDown(GLFW_KEY_RIGHT, true)) {
			setSelectedTileTypeIndex(selectedTileTypeIndex + 1);
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_LEFT) && !KeyboardInput.isKeyDown(GLFW_KEY_LEFT, true)) {
			setSelectedTileTypeIndex(selectedTileTypeIndex - 1);
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_S)) {
			player.activateCommandMode("drawsquare");
		}

		if (MouseInput.isButtonDown(0)) {
			InGame.map.getMousePosInMap(targetCell);
			InGame.map.SetTileType(targetCell, getSelectedTileType());
			return;
		}

		if (MouseInput.isButtonDown(1)) {
			InGame.map.getMousePosInMap(targetCell);
			InGame.map.SetTileType(targetCell, 0);
			return;
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_A) && !KeyboardInput.isKeyDown(GLFW_KEY_A, true)) {
			InGame.map.getMousePosInMap(targetCell);
			if (entityToSpawn.equals("player")) {
				InGame.map.spawns.add(new PlayerSpawn(new Vector2i(targetCell.x, targetCell.y)));
			} else if(spawnItem) {
				InGame.map.spawns.add(new ItemSpawn(new Vector2i(targetCell.x, targetCell.y), entityToSpawn));
			} else {
				InGame.map.spawns.add(new ActorSpawn(new Vector2i(targetCell.x, targetCell.y), entityToSpawn, squad));
			}
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_Z)) {
			InGame.map.getMousePosInMap(targetCell);
			ArrayList<EntitySpawn> spawnsToRemove = new ArrayList<EntitySpawn>();
			for(EntitySpawn spawn : InGame.map.spawns) {
				if ((spawn instanceof ActorSpawn || spawn instanceof PlayerSpawn) && spawn.position.equals(targetCell)) {
					spawnsToRemove.add(spawn);
				}
			}
			
			for (EntitySpawn spawn : spawnsToRemove) {
				InGame.map.spawns.remove(spawn);
			}
			flagPool.hideAll();
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_Q)) {
			player.activateCommandMode("selectspawnflag");
		}
		
		for (int i = 0; i <= 9; i++) {
			if (KeyboardInput.isKeyDown(i + GLFW_KEY_1)) {
				squad = i + 1;
			}
		}
	}

	@Override
	public void activate() {
		player.cameraLock = false;

	}
	
	public int getSelectedTileType() {
		return tileTypes.get(selectedTileTypeIndex).intValue();
	}
	
	private void setSelectedTileTypeIndex(int index) {
		selectedTileTypeIndex = Math.max(0, Math.min(index, tileTypes.size() - 1));
		tileSelectionDisplay.setSelectedOption(selectedTileTypeIndex);
	}
	
	Vector2f checkDirectionInput() {
		for (int key : Directions.map.keySet()) {
			if (KeyboardInput.isKeyDown(key)) {
				return new Vector2f(Directions.Get(key));
			}
		}
		return new Vector2f(0, 0);
	}

}
