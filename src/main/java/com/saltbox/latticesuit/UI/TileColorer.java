package com.saltbox.latticesuit.UI;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.gamestates.InGame;

public class TileColorer {
	static ArrayList<Vector2f> tiles;
	static Vector3f color;
	static Vector3f defaultColor = new Vector3f(1, 1, 1);
	public static void SetTiles(ArrayList<Vector2f> tiles) {
		TileColorer.tiles = tiles;
	}
	
	public static void SetColor(Vector3f color) {
		TileColorer.color = color;
	}
	
	public static void apply() {
		colorTiles(color);
	}
	
	public static void clear() {
		colorTiles(defaultColor);
	}
	
	private static void colorTiles(Vector3f color) {
		for (Vector2f v : tiles) {
			InGame.map.bufColor(Math.round(v.x), Math.round(v.y), color);
		}
		InGame.map.pushBuffers();
	}
}
