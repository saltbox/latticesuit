package com.saltbox.latticesuit.entities.actors;

import org.joml.Vector2f;
import org.joml.Vector2i;

import com.saltbox.latticesuit.entities.Entity;

public class TargetDummy extends Actor {

	public TargetDummy(Vector2f position) {
		super(position);
		targetable = true;
		damageable = true;
		raycastable = true;
		name = "target dummy";
		setSprite(new Vector2i(15, 1));
		health = 9;
	}

}
