package com.saltbox.latticesuit.entities.actors;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.AIStates.*;
import com.saltbox.latticesuit.battlegroup.*;
import com.saltbox.latticesuit.databases.DatabaseMediator;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.items.Weapon;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.logging.Logger;

public class Soldier extends AIActor {
	public Soldier(Vector2f position, Fireteam fireteam) {
		super(position);
		setSprite('S');
		setColor(new Vector3f(1, 0, 0));
		targetable = true;
		raycastable = true;
		damageable = true;
		health = 30;
		AIStates.put("idle", new Idle(this));
		AIStates.put("assault", new Assault(this));
		AIStates.put("firefight", new Firefight(this));
		AIStates.put("firefightreposition", new FirefightReposition(this));
		setState("idle");
	}
	
	public Soldier(Vector2f position) {
		this(position, null);
	}
	
	
	@Override
	public void Update(GameTime gameTime) {
		super.Update(gameTime);
	}
}
