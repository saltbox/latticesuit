package com.saltbox.latticesuit;

import java.util.ArrayList;

import org.joml.Vector2f;

import com.saltbox.latticesuit.entities.Entity;

public class RaycastHit {
	public Entity hitEntity;
	public Vector2f hitPosition;
	public ArrayList<Entity> hitEntities = new ArrayList<Entity>();
	public int remainingDamage;

	public boolean hitMultiple() {
		return hitEntities.size() > 1;
	}
}
