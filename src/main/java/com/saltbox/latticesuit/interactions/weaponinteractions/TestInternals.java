package com.saltbox.latticesuit.interactions.weaponinteractions;

import com.saltbox.latticesuit.UI.GameLog;
import com.saltbox.latticesuit.interactions.Interaction;
import com.saltbox.latticesuit.items.Weapon;

public class TestInternals extends Interaction {
	Weapon weapon; 
	public TestInternals(Weapon w) {
		super("test internals");
		weapon = w;
	}

	@Override
	public void doInteraction() {
		GameLog.writeLine(weapon.getCondition());
	}
	
	@Override
	public int getFrames() {
		return weapon.internalsTestTime;
	}

}
