package com.saltbox.latticesuit.entities.actors;

import java.util.ArrayList;
import java.util.HashMap;

import org.joml.Vector2f;

import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.Raycast;
import com.saltbox.latticesuit.RaycastHit;
import com.saltbox.latticesuit.AIStates.AIState;
import com.saltbox.latticesuit.battlegroup.Fireteam;
import com.saltbox.latticesuit.battlegroup.IFireteamMember;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.Map;
import com.saltbox.latticesuit.gamestates.InGame;
import com.saltbox.latticesuit.items.Weapon;
import com.saltboxgames.latticelight.GameTime;

public abstract class AIActor extends Actor implements IFireteamMember{
	protected HashMap<String, AIState> AIStates;
	protected Fireteam fireteam;
	protected Fireteam previousFireteam;
	public Vector2f targetPosition = null;
	
	public AIState activeState;
	public AIActor(Vector2f position) {
		super(position);
		AIStates = new HashMap<String, AIState>();
		raycastMask = raycastMask | Raycast.Mode.ENEMY.maskIndex;
	}
	
	public void Update(GameTime gt) {
		super.Update(gt);
		if (actionFrames <= 0) {
			activeState.Update(gt);
		}
	}
	
	public void setState(String newState) {
		activeState = AIStates.get(newState);
		activeState.Start();
	}

	@Override
	public boolean isAlive() {
		return health > 0;
	}
	
	@Override
	public void Damage(int damage) {
		super.Damage(damage);
	}

	public void setFireteam(Fireteam fireteam) {
		previousFireteam = this.fireteam;
		this.fireteam = fireteam;
		if (previousFireteam != null) {
			previousFireteam.members.remove(this);
		}
		fireteam.members.add(this);
	}

	@Override
	public Fireteam getFireteam() {
		return fireteam;
	}
	
	Vector2f moveDirection = new Vector2f();
	
	
	
	public Vector2f getTargetPosition() 
	{
		return targetPosition;
	} 
	
	public void lookOutForPlayer() {
		Entity e = InGame.player;
		RaycastHit hit = Raycast.raycast(getPosition(), e.getPosition(), null, this, Raycast.Mode.VISION.maskIndex | Raycast.Mode.PLAYER.maskIndex | Raycast.Mode.ENEMY.maskIndex);
		if (hit != null && hit.hitEntity != null && hit.hitEntity.equals(e)) {
			setTargetPosition(hit.hitEntity.getPosition(), true);
		}
	}

	@Override
	public AIState getActiveState() {
		return activeState;
	}

	@Override
	public boolean canSeeTarget() {
		RaycastHit hit = Raycast.raycast(getPosition(), targetPosition, null, this, Raycast.Mode.VISION.maskIndex | Raycast.Mode.PLAYER.maskIndex);
		return hit != null && hit.hitEntity != null && hit.hitEntity.equals(InGame.player);
	}
	
	@Override
	public boolean noAlliesInFrontOfTarget() {
		RaycastHit hit = Raycast.raycast(getPosition(), targetPosition, null, this, Raycast.Mode.VISION.maskIndex | Raycast.Mode.PLAYER.maskIndex | Raycast.Mode.ENEMY.maskIndex);
		return hit != null && hit.hitEntity != null && hit.hitEntity.equals(InGame.player);
	}
	
	public void setTargetPosition(Vector2f newPosition, boolean notifyFireteam) {
		targetPosition = new Vector2f(newPosition);
		if (notifyFireteam) {
			fireteam.updateTargetPosition(newPosition);
		}
	}

	public void attackWithWeapon(Vector2f target) {
		Weapon f = (Weapon)getPrimaryWeapon();
		if (f.magStatus > 0) {
			ArrayList<Vector2f> cells = new ArrayList<Vector2f>();
			Attack(getPosition(), target, cells);
			InGame.shotDisplayer.addNewShot(cells, this);
		} else {
			f.reload();
			TakeTurn(f.reloadTime);
		}
	}
}
