package com.saltbox.latticesuit.playercommands;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.LatticeSuit;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.UI.PlayerUI;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.input.MouseInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public abstract class InventoryManagementCommand extends PlayerCommand {
	public boolean confirmSelection = false;
	public boolean maintainPositionInInactiveMenus = false;
	
	protected ArrayList<Menu> menus;
	protected ArrayList<ArrayList<Entity>> menuContentArrays;
	Vector2f origin;
	Vector2f drawOffset;
	protected Vector2f menuSize;
	protected int selectedInventory = 0;
	protected int selectedRow = 0;
	
	public InventoryManagementCommand(Player player) {
		super(player);
		menus = new ArrayList<Menu>();
		drawOffset = new Vector2f();
		menuSize = new Vector2f(24, 20);
		origin = new Vector2f(0, LatticeSuit.GetSize().y - (PlayerUI.weaponLines + menuSize.y));
		menuContentArrays = new ArrayList<ArrayList<Entity>>();
	}
	
	public void addMenu(ArrayList<Entity> content) {
		menuContentArrays.add(content);
		Menu newMenu = new Menu(origin);
		for (Entity e : content) {
			if (e != null) {
				newMenu.options.add(e.getName());
			}
		}
		newMenu.selected = false;
		menus.add(newMenu);
	}
	
	public void autoUpdateMenuContents() {
		for (int i = 0; i < menuContentArrays.size(); i++) {
			menus.get(i).options.clear();
			for (Entity e : menuContentArrays.get(i)) {
				if (e != null) {
					menus.get(i).options.add(e.getName());
				}
			}
		}
	}

	@Override
	public void draw(SpriteBatch sb) {
		for (int i = 0; i < menus.size(); i++) {
			drawOffset.set(menuSize.x * i, 0).add(origin);
			menus.get(i).setPosition(drawOffset);
			menus.get(i).Draw(sb);
		}
	}

	@Override
	public boolean doCommand() {
		if (confirmSelection) {
			confirmSelection();
		}
		return false;
	}
	
	public void confirmSelection() {
		confirmSelection = false;
		autoUpdateMenuContents();
	}

	@Override
	public void update(GameTime gameTime) {
		for (int i = 0; i < menus.size(); i++) {
			if (menus.get(i).clickedOption() > -1 && MouseInput.isButtonDown(GLFW_MOUSE_BUTTON_1)) {
				changeSelectedInventory(i);
			}
			if (i == selectedInventory) {				
				menus.get(i).Update(gameTime);
				selectedRow = menus.get(i).getSelectedOption();
			}
		}
		
		if (KeyboardInputTools.confirm()) {
			confirmSelection = true;
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE)) {
			player.activateCommandMode("default");
			player.cursor.active = false;
		}
		
		if (KeyboardInput.isKeyDown(GLFW_KEY_KP_6) || KeyboardInput.isKeyDown(GLFW_KEY_RIGHT)) {
			changeSelectedInventory(selectedInventory + 1);
		}
		if (KeyboardInput.isKeyDown(GLFW_KEY_KP_4) || KeyboardInput.isKeyDown(GLFW_KEY_LEFT)) {
			changeSelectedInventory(selectedInventory - 1);
		}
	}

	private void changeSelectedInventory(int newSelection) {
		newSelection = Math.max(0, Math.min(menus.size() - 1, newSelection));
		if (newSelection == selectedInventory) {
			return;
		}
		int previousSelection = selectedInventory;
		selectedInventory = newSelection;
		
		menus.get(previousSelection).selected = false;
		menus.get(newSelection).selected = true;
		
		if (maintainPositionInInactiveMenus) {
			menus.get(newSelection).setSelectedOption(0);
		} else {
			menus.get(newSelection).setSelectedOption(Math.max(0, Math.min(menus.get(previousSelection).getSelectedOption(), menus.get(selectedInventory).options.size() - 1)));
			menus.get(previousSelection).setSelectedOption(-1);
		}
		
		selectedRow = menus.get(newSelection).getSelectedOption();
	}

	@Override
	public void activate() {
		autoUpdateMenuContents();
		selectedInventory = 0;
		selectedRow = 0;
		menus.get(0).setSelectedOption(0);
		for (int i = 1; i < menus.size(); i++) {
			menus.get(i).setSelectedOption(-1);
		}
	}
	
	public int getSelectedItemRow() {
		return menus.get(selectedInventory).getSelectedOption();
	}
}
