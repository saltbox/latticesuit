package com.saltbox.latticesuit.databases;

public class DatabaseMediator {
	public static ItemDatabase items;
	public static TileDatabase tiles;
	public static StringDatabase strings;
	public static MapDatabase maps;
	public static ActorDatabase actors;
	
	public static void CreateDatabases() {
		items = new ItemDatabase();
		tiles = new TileDatabase();
		strings = new StringDatabase();
		maps = new MapDatabase();
		actors = new ActorDatabase();
	}
}
