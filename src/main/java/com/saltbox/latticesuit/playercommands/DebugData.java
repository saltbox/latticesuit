package com.saltbox.latticesuit.playercommands;

import com.saltbox.latticesuit.KeyboardInputTools;
import com.saltbox.latticesuit.UI.IOptionColorer;
import com.saltbox.latticesuit.UI.Menu;
import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.input.KeyboardInput;
import com.saltboxgames.latticelight.rendering.SpriteBatch;
import static org.lwjgl.glfw.GLFW.*;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

public class DebugData extends PlayerCommand {
	Menu debugMenu;
	Stack<Object> objectStack = new Stack<Object>();
	Object[] objectsInCollection = null;
	ArrayList<Field> fields = new ArrayList<Field>();
	public DebugData(Player player) {
		super(player);
		debugMenu = new Menu(new Vector2f(0, 1), new Vector2i(70, 50));
		debugMenu.optionColorer = new DebugOptionColorer();
	}
	
	@Override
	public void draw(SpriteBatch sb) {
		debugMenu.Draw(sb);
	}

	@Override
	public boolean doCommand() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(GameTime gameTime) {
		debugMenu.Update(gameTime);
		if (KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE) && !KeyboardInput.isKeyDown(GLFW_KEY_ESCAPE, true)) {
			if (objectStack.size() <= 1) {
				objectStack.pop();
				player.activateCommandMode("default");
			} else {
				objectStack.pop();
				debugObject(objectStack.peek(), false);
			}
		}
		
		if (debugMenu.getSelectedOption() > 0 && KeyboardInputTools.confirm()) {
			try {
				if (objectsInCollection != null) {
					if (objectsInCollection[debugMenu.getSelectedOption() - 1] != null) {
						debugObject(objectsInCollection[debugMenu.getSelectedOption() - 1], true);
					}
				} else {
					debugObject(fields.get(debugMenu.getSelectedOption() - 1).get(objectStack.peek()), true);
				}
			} catch (IllegalArgumentException | IllegalAccessException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void activate() {
		
	}
	
	public void debugObject(Object o, boolean pushToStack) {
		objectsInCollection = null;
		if (pushToStack) {
			objectStack.push(o);
		}
		fields.clear();
		player.activateCommandMode("debugdata");
		debugMenu.options.clear();
		debugMenu.setSelectedOption(0);
		
		debugMenu.options.add(o.getClass().getSimpleName());
		if (o instanceof Collection) {
			objectsInCollection = ((Collection<?>)(o)).toArray();
			for (Object objectInCollection: objectsInCollection) {
				if (objectInCollection != null) {
					debugMenu.options.add(objectInCollection.getClass().getSimpleName() + ": " + objectInCollection.toString());
				} else {
					debugMenu.options.add("null");
				}
			}
		} else {
			Class<?> targetClass = o.getClass();
			addClassFieldsToOptions(o, targetClass, false);
			targetClass = targetClass.getSuperclass();
			while (targetClass != null) {
				addClassFieldsToOptions(o, targetClass, true);
				targetClass = targetClass.getSuperclass();
			}
		}
		
	}	

	private void addClassFieldsToOptions(Object o, Class<?> targetClass, boolean belowProtectedOnly) {
		for (Field f : targetClass.getFields()) {
			if (!belowProtectedOnly || (!Modifier.isProtected(f.getModifiers()) && !Modifier.isPublic(f.getModifiers()))) {
				addFieldToOptions(o, f);
			}
		}
	}

	private void addFieldToOptions(Object o, Field f) {
		f.setAccessible(true);
		fields.add(f);
		try {
			if (f.get(o) != null) {
				debugMenu.options.add(f.getName() + ": " + f.get(o).toString());
			} else {
				debugMenu.options.add(f.getName() + ": " + "null");
			}
		} catch (IllegalArgumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		f.setAccessible(false);
	}
}

class DebugOptionColorer implements IOptionColorer {
	public Vector3f highlightColor = new Vector3f(0, 1, 0);
	public Vector3f valueHighlightColor = new Vector3f(0.16f, 0.8f, 0.737f);
	public Vector3f defaultColor = new Vector3f(1f, 1f, 1f);
	
	@Override
	public void runOn(Menu menu, int optionIndex, Vector2i optionPosition) {
		Vector3f colorToUse;
		if (optionIndex == menu.getSelectedOption()) {
			colorToUse = highlightColor;
		} else {
			colorToUse = defaultColor;
		}
		Vector2i menuSize = new Vector2i();
		menu.getSize(menuSize);
		String optionText = menu.options.get(optionIndex);
		for (int x = 0; x < menuSize.x; x++) {
			menu.bufColor(x, optionPosition.y, colorToUse);
			if (x < optionText.length() && optionText.charAt(x) == ':') {
				colorToUse = valueHighlightColor;
			}
		}
		menu.pushBuffers();
	}
}
