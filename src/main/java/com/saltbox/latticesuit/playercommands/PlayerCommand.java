package com.saltbox.latticesuit.playercommands;

import com.saltbox.latticesuit.entities.actors.Player;
import com.saltboxgames.latticelight.GameTime;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public abstract class PlayerCommand {
	public String name;
	
	protected Player player;

	public PlayerCommand(Player player) {
		this.player = player;
	}

	public abstract void draw(SpriteBatch sb);

	public abstract boolean doCommand();

	public abstract void update(GameTime gameTime);

	public abstract void activate();

	// public abstract void close();
}
