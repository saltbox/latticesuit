package com.saltbox.latticesuit.interactions;

import java.util.ArrayList;

import com.saltbox.latticesuit.entities.Entity;
import com.saltbox.latticesuit.interactions.weaponinteractions.CheckAmmo;
import com.saltbox.latticesuit.interactions.weaponinteractions.FieldRepair;
import com.saltbox.latticesuit.interactions.weaponinteractions.Reload;
import com.saltbox.latticesuit.interactions.weaponinteractions.TestInternals;
import com.saltbox.latticesuit.items.Weapon;

public class Interactions {
	
	public static ArrayList<Interaction> baseSet(Entity e) {
		ArrayList<Interaction> i = new ArrayList<Interaction>();
		i.add(new LookAt(e));
		i.add(new Destroy(e));
		return i;
	}
	
	public static ArrayList<Interaction> firearmSet(Weapon w) {
		ArrayList<Interaction> i = new ArrayList<Interaction>();
		i.add(new CheckAmmo(w));
		i.add(new Reload(w));
		i.add(new TestInternals(w));
		i.add(new FieldRepair(w));
		
		return i;
	}
}
