package com.saltbox.latticesuit.entities;

import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;

import com.saltbox.latticesuit.CommandTile;
import com.saltboxgames.latticelight.logging.Logger;
import com.saltboxgames.latticelight.rendering.SpriteBatch;

public class TilePool {
	ArrayList<CommandTile> tilePool;
	char tileCharacter;
	Vector3f tileColor;

	public TilePool(char character, Vector3f color) {
		this.tileCharacter = character;
		this.tileColor = color;
		this.tilePool = new ArrayList<CommandTile>();
	}

	public CommandTile get(int index) {
		if (tilePool.size() <= index) {
			for (int i = tilePool.size(); i <= index; i++) {
				tilePool.add(new CommandTile(new Vector2f(0, 0), tileCharacter, tileColor));
			}
		}
		return tilePool.get(index);
	}

	public int size() {
		return tilePool.size();
	}

	public void hideAll() {
		for (int i = 0; i < tilePool.size(); i++) {
			tilePool.get(i).active = false;
		}
	}

	public void Draw(SpriteBatch sb) {
		for (CommandTile commandTile : tilePool) {
			if (commandTile.active) {
				commandTile.Draw(sb);
			}
		}
	}
}
